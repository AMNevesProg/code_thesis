# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:44:31 2019

@author: Ricardo
"""

#Extract cluster data


import os
import re
import sys
import Geometries_FGC, VegetationIndexes, resampleTo10m, imageAlign
from osgeo import gdal, ogr,osr
import numpy as np
import traceback
import pandas as pd
from timeit import default_timer as timer
import multiprocessing
from multiprocessing import Process,Pool, Queue
import threading

##com isto deixa de dar o runtimerror quanto é feito GetNextFeature
ogr.DontUseExceptions()       
gdal.UseExceptions()


def rasterToArray(file):
     raster= gdal.Open(file)
     band = raster.GetRasterBand(1)
     arr = np.array(band.ReadAsArray(0,0,raster.RasterXSize,raster.RasterYSize))
     return arr[arr[:,:] > 0]


def extract_cluster_s1_parallel(fgc_dir,folder, granule_path, geoms_fgc,geoms_buff):                                 
    Geometries_FGC.cut_v3(granule_path, geoms_buff, fgc_dir+"/"+folder+"/s1/", "buffer", folder)
    Geometries_FGC.cut_v3(granule_path, geoms_fgc, fgc_dir+"/"+folder+"/s1/", "fgc", folder)    
                

def parallel_extract_s1(info):
    fgc_dir = info[0]
    folder = info[1]
    img_paths = info[2]
    
    for p in img_paths:
        extract_cluster_s1_parallel(fgc_dir,folder,p, info[3], info[4])

def extract_cluster_s2_parallel(fgc_dir,folder, granule_path, geoms_fgc,geoms_buff):
    Geometries_FGC.cut_v3(granule_path, geoms_buff, fgc_dir+"/"+folder+"/s2/", "buffer", folder)
    Geometries_FGC.cut_v3(granule_path, geoms_fgc, fgc_dir+"/"+folder+"/s2/", "fgc", folder)
    
    
def parallel_extract_s2(info):
    fgc_dir = info[0]
    folder = info[1]
    
    img_paths = info[2]
    
    for p in img_paths:
        extract_cluster_s2_parallel(fgc_dir,folder,p, info[3], info[4])
   


def start_parallel_extract(fgc_dir,folder,sat, img_folder):
#    if "s2" in sat:
#        path = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/Sentinel-2/"
#    else:   
#        path = "E:/rafon/Documents/storage/Dados Tese/Cardigos/2018/Sentinel-1/processado_db/"
    
    try:
        os.mkdir(fgc_dir+"/"+folder+"/s1")
    except:
        print("Satellite s1 data folder for ", folder, " already exists!")
    
    try:
        os.mkdir(fgc_dir+"/"+folder+"/s2")
    except:
        print("Satellite s2 data folder for ", folder, " already exists!")
    
    granules = []   
    
    for subdir, dirs, files in os.walk(img_folder):
        for file in files:
            file_path = subdir+'/'+file
          
            if "T29" in subdir and "IMG_DATA" in subdir and "aligned" in file and (file.endswith(".jp2") or file.endswith(".tiff") ):     
                granules.append(file_path)
                
            elif (file.endswith(".tiff") or file.endswith(".img")) and "Beta0" in file:
                granules.append(file_path)
    
    
    geoms_fgc, geoms_buff = Geometries_FGC.get_geoms(fgc_dir, folder, sat)
    
    threads = multiprocessing.cpu_count()   
    threads_info = []
    thread = 0
    for g in granules:
        
        thread = thread % threads
        img_path = np.array([g])
          
        if len(threads_info) <= thread:    # cria uma nova entrada para cada thread
            temp = np.array([[fgc_dir,folder , img_path, geoms_fgc.copy(), geoms_buff.copy()]] )
            if len(threads_info) == 0:
                threads_info = temp
            else:
                threads_info = np.concatenate((threads_info, temp))
        else:
            threads_info[thread,2] = np.append(threads_info[thread,2], img_path) # adiciona a imagem a lista de imagens que ja existe
            
        thread = thread +1
    
    #print(threads_info[:,0:3])
    pool = multiprocessing.Pool(processes=threads)
    if "s1" in sat:    
        pool.map( parallel_extract_s1,threads_info ) 
    else:
        pool.map( parallel_extract_s2,threads_info )
        
    pool.close()
    pool.join()
    pool = None
        
    

def reproj_all_in_folder( folder, sat,ref):

    if not os.path.isdir(folder+"new_proj/"+sat) :
        try:
            os.mkdir(folder+"new_proj")
        except:
            pass
        try:
            os.mkdir(folder+"new_proj/"+sat)
        except:
            pass
            
        inDataSet = gdal.Open(ref)
        proj = inDataSet.GetProjectionRef()
        print(proj)
        for subdir, dirs, files in os.walk(folder):
            for file in files:
                
                file_path = subdir+'/'+file
                if file.endswith(".shp") and "new_proj" not in subdir and not os.path.exists(subdir+"/new_proj/"+sat+"/"+file):
                    Geometries_FGC.reproject_file(file_path,proj, subdir+"/new_proj/"+sat+"/"+file)
    else:
        print("Files already prepared!")


def check_if_aligned(folder):

    not_aligned = []
    for subdir, dirs, files in os.walk(folder):
        for file in files:
             if file.endswith("10m.jp2") and not file.startswith("aligned"):
                 product = re.findall(r"[\\/]{1}S2.*\.SAFE[\\/]{1}", str(subdir))[0][1:-1]
                 not_aligned.append(product)
    
    return np.unique(np.asarray(not_aligned))


def check_if_resampled(folder):
    images_folders = []
    for name in os.listdir(folder):
        if os.path.isdir(folder+"/"+name):
            images_folders.append(name)    
    
    resampled = set()                  
    for subdir, dirs, files in os.walk(folder):
        for file in files:
             file_path = subdir+'/'+file
             if "R10m" in file_path and file.endswith(".jp2") and ("B05_" in file or "B06_" in file):  
                 result = re.findall(r"[\\/]{1}S2.*\.SAFE[\\/]{1}", str(subdir))[0][1:-1]
                 resampled.add(result)
                  
    images_folders = np.asarray(images_folders)         
#    resampled = np.asarray(resampled)
    final = []
    for folder in images_folders:
        if folder not in resampled:
            final.append(folder)

    return np.unique(final)
    
def check_vegetation_indexes(folder):
    images_folders = []
    for name in os.listdir(folder):
        if os.path.isdir(folder+"/"+name):
            images_folders.append(name)    
    images_folders = np.asarray(images_folders)
    
    resampled = set()                  
    for subdir, dirs, files in os.walk(folder):
        NDVI = SR = IRECI = NDWI = SAVI = False
        
        for file in files:
            file_path = subdir+'/'+file
            if "_NDVI" in file:
                NDVI = True  
            if "_SAVI" in file:
                SAVI = True    
            if "_SR" in file:
                SR = True     
            if "_NDWI" in file:
                NDWI= True
            if "_IRECI" in file:
                IRECI= True
             
            if  NDWI and NDVI and SR and IRECI and SAVI:  
                result = re.findall(r"[\\/]{1}S2.*\.SAFE[\\/]{1}", str(subdir))[0][1:-1]
                images_folders = images_folders[images_folders != result]
                  

    return images_folders
    

def prepare_files_to_parallel(fgc_dir, s1_folder, s2_folder):
    #escolher uma imagem para copiar a projecção, para depois reprojetar os shapefiles para a mesma projecção
    for subdir, dirs, files in os.walk(s2_folder):
        for file in files:
            file_path = subdir+'/'+file
            if "T29" in subdir and "IMG_DATA" in subdir and "TCI" in file  and (file.endswith(".jp2") or file.endswith(".tiff") ):     
                ref_s2_img = file_path 
                break
            
    for subdir, dirs, files in os.walk(s1_folder):
        for file in files:
            file_path = subdir+'/'+file
            if (file.endswith(".tiff") or file.endswith(".img")) and "_VV" in file:     
                ref_s1_img = file_path


    reproj_all_in_folder(fgc_dir+"/estradas/clusters_data/","s1",ref_s1_img)
    reproj_all_in_folder(fgc_dir+"/linhas/clusters_data/","s1",ref_s1_img)
    reproj_all_in_folder(fgc_dir+"/habitacoes/clusters_data/","s1",ref_s1_img)
    
    reproj_all_in_folder(fgc_dir+"/estradas/clusters_data/","s2",ref_s2_img)
    reproj_all_in_folder(fgc_dir+"/linhas/clusters_data/","s2",ref_s2_img)
    reproj_all_in_folder(fgc_dir+"/habitacoes/clusters_data/","s2",ref_s2_img)
 
    
def remove_unaligned_images(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            file_path = subdir+'/'+file
            if not file.startswith("aligned_") and (file.endswith("m.jp2") or file.endswith("m.tiff")):
                os.remove(file_path)    


def start(fgc_dir,s1_folder, s2_folder):
    
#    s1_folder = "E:/rafon/Documents/storage/Dados Tese/Cardigos/2018/Sentinel-1/processado/"
#    s2_folder = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/Sentinel-2/"
    
    
    files = check_if_aligned(s2_folder)
    if len(files) != 0:
        print("There are products that aren't aligned! Aligning...")
        for folder in files:
            imageAlign.align_all_images(s2_folder+"/"+folder)
            remove_unaligned_images(s2_folder+"/"+folder)
            
    files = check_if_resampled(s2_folder)    
    if len(files) != 0:
        print("There are products that aren't resampled! Resampling...")
        for folder in files:
            resampleTo10m.resample_all(s2_folder+"/"+folder)
               
    
    files =  check_vegetation_indexes(s2_folder)
    if len(files) != 0:
        print("There are vegetation indexes missing! Calculating...")
        for folder in files:
            VegetationIndexes.indexes_to_all(s2_folder+"/"+folder)
        
    
#    
    print("Preparing files to parallel processing...")
    prepare_files_to_parallel(fgc_dir, s1_folder, s2_folder)
    
    
    print("Starting to cut images..")
    start = timer()
    start_parallel_extract(fgc_dir, "estradas", "s1", s1_folder)
    duration = timer() - start
    print("Duration s1 estradas: ", duration)
    
    start = timer()
    start_parallel_extract(fgc_dir, "estradas", "s2",s2_folder)
    duration = timer() - start
    print("Duration s2 estradas: ", duration)


    start = timer()
    start_parallel_extract(fgc_dir, "linhas", "s1",s1_folder)
    duration = timer() - start
    print("Duration s1 linhas: ", duration)
    
    start = timer()
    start_parallel_extract(fgc_dir, "linhas", "s2", s2_folder)
    duration = timer() - start
    print("Duration s2 linhas: ", duration)



        
if __name__ == '__main__': 

  
    if len(sys.argv) != 4 :
        print("Missing arguments! Command usage:")
        print("   python ExtractClustersData.py '<path_to_fgci_directory>' '<sentinel1_products_directory>' '<sentinel2_products_directory>'\n")
        sys.exit()

    fgc_dir = sys.argv[1]
    s1_folder = sys.argv[2]
    s2_folder = sys.argv[3]
    
    
    s1_folder = "E:/rafon/Documents/storage/Dados Tese/Cardigos/2018/Sentinel-1/processado/"
    s2_folder = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/Sentinel-2/"
    
#    fgc_dir = "../../Santarem/fgc"
    fgc_dir = "../../exp/series/clusters/data/"
    
    start(fgc_dir, s1_folder, s2_folder)    















                