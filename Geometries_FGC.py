# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 14:34:49 2019

@author: Ricardo
"""
import os
import sys
import gc
import zipfile
from timeit import default_timer as timer
from osgeo import gdal, ogr,osr
from concavehull import ConcaveHull
import numpy as np
import pandas as pd
import traceback
import multiprocessing
from multiprocessing import Process,Pool, Queue
import subprocess 
import fiona
import rasterio
from rasterio.mask import mask
import DataExtration
import re
import ntpath
     
gdal.UseExceptions()
ogr.UseExceptions()

#cria na layer dest os fields da layer src
def createFieldsFrom(src, dest):
    lyrDefn = src.GetLayerDefn()

    for i in range( lyrDefn.GetFieldCount() ):
        dest.CreateField(lyrDefn.GetFieldDefn(i))
#        fieldName =  lyrDefn.GetFieldDefn(i).GetName()
    
    return dest 


def addBuffer(file,size, new_file):

    fgc_shp = ogr.Open(file)
    layer = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(new_file)

    out_lyr=ds.CreateLayer('temp', layer.GetSpatialRef(), ogr.wkbPolygon)
    
    out_lyr = createFieldsFrom(layer, out_lyr)
    
    for feature in layer:
        geom = feature.GetGeometryRef()
        
        if geom != None:
            new_feat = ogr.Feature(out_lyr.GetLayerDefn())
            new_feat.SetFrom(feature)
            new_feat.SetGeometry(geom.Buffer(size))
            
            
            out_lyr.CreateFeature(new_feat)
        # else:
            #out_lyr.CreateFeature(feature)
                        
            new_feat = None
            geom = None
                    
    out_lyr= None
    ds=None
    driver = None
    layer = None  
    fgc_shp  = None          
                
def merge_geometries_by_field(file,out, field):
    try:
        file_name = os.path.basename(file)
        
        if field == None :
            query = "SELECT ST_Union(geometry) AS geometry FROM '" + file_name[:-4]+"'"
        else:
            query = "SELECT ST_Union(geometry) AS geometry, "+field+" FROM '" + file_name[:-4]+"' GROUP BY "+field
             
        
        print(file)
        ds = gdal.VectorTranslate(out,file,
                             SQLDialect="sqlite",
                             SQLStatement=query,
                             format="ESRI Shapefile",
                             geometryType="PROMOTE_TO_MULTI")
        ds = None
    except:
        traceback.print_exc()
                       
                
                
def decompressAllZip():
    path = "E:/rafon/Documents/storage/Dados Tese/FGCI_Portugal"
    
    for subdir, dirs, files in os.walk(path):
        contains_shp = False
        zip_file = ""
        for file in files:
            if file.endswith(".shp"):
                contains_shp = True
            if file.endswith(".zip"):
                zip_file = subdir + "\\" + file
            
        if not contains_shp and zip_file != "": 
            print(zip_file)
            zip_ref = zipfile.ZipFile(zip_file, 'r')
            zip_ref.extractall(subdir)
            zip_ref.close()
         
            
            
def cut(raster, shapefile, out):
    start = timer()
    gdal.SetConfigOption('GDALWARP_IGNORE_BAD_CUTLINE', 'YES')
    
    fgc_shp = ogr.Open(shapefile)
    lyr1 = fgc_shp.GetLayer()
    feat =  lyr1.GetNextFeature()
    geom = None
    if feat != None:
        geom = feat.GetGeometryRef()
#    geom=1
    if geom != None:      
        try:
            ds = gdal.Warp(out,raster, 
                           format='GTiff',
    #                           xRes = 10.0, yRes= -10.0,
    #                           srcSRS = "ESPG:32629", 
                           targetAlignedPixels =False,
                           cropToCutline =True, 
                           resampleAlg ="bilinear",
                           cutlineDSName = shapefile,
                           multithread =True,
                           dstNodata = 0, options =["GDALWARP_IGNORE_BAD_CUTLINE"])
            ds=None
            fgc_shp = None
    #        duration = timer() - start
            print("Duration: ",duration)
        
        except:
            fgc_shp = None
            lyr1 = None
            feat = None
#            print("exception thrown!")
#            traceback.print_exc()
    else:
        print("NOneD")
        
def cut_v2(raster, shapefile, out):
    
#    start = timer()
    with fiona.open(shapefile) as shape:
        geoms = [feature["geometry"] for feature in shape]
        

    if len(geoms) >0 and geoms[0] != None:

        with rasterio.open(raster) as src:
            out_image, out_transform = mask(src, geoms, crop=True)
        out_meta = src.meta.copy()
        
        # save the resulting raster  
        out_meta.update({"driver": "GTiff",
            "height": out_image.shape[1],
            "width": out_image.shape[2],
        "transform": out_transform})
        
        with rasterio.open(out, "w", **out_meta) as dest:
            dest.write(out_image)
            
            
def cut_v3(raster, geoms_all, out_folder, fgc_type, fgc):
    if fgc_type == "buffer":
         file_prefix = "buffer_20m_"
    else:
         file_prefix = "fgc_"+fgc+"_"
    
    with rasterio.open(raster) as src:
 
        for c_id in geoms_all.keys():
            geoms = geoms_all[c_id]
            
            if len(geoms) >0 and geoms[0] != None:
            
                out_image, out_transform = mask(src, geoms, crop=True)
                out_meta = src.meta.copy()
                
                # save the resulting raster  
                out_meta.update({"driver": "GTiff",
                    "height": out_image.shape[1],
                    "width": out_image.shape[2],
                "transform": out_transform})
                
                cluster_id = c_id.split("_")[1]
                
                try:
                    os.mkdir(out_folder+str(cluster_id))
                except:
                    pass
                        
            
                if "/s1/" in out_folder:
                    image_date = re.findall("[0-9]{8}T[0-9]*", raster)[0]
                    out = out_folder+str(cluster_id)+"/"+file_prefix+str(cluster_id)+"_"+ image_date+"_"+ntpath.basename(raster)
                    
                elif "/s2/" in out_folder:
                    out = out_folder+str(cluster_id)+"/"+file_prefix+str(cluster_id)+"_"+ntpath.basename(raster)
                
                with rasterio.open(out, "w", **out_meta) as dest:
                    dest.write(out_image)
        


def get_geoms(fgc_dir, fgc, sat): 
    
    geoms_fgc = {}
    geoms_buffer= {}
    
    for subdir, dirs, files in os.walk(fgc_dir+"/"+fgc+"/clusters_data/new_proj/"+sat):
        for file in files:
            file_path = subdir+'/'+file
            if ".shp" in file:
                
                with fiona.open(file_path) as shape:
                    geoms = [feature["geometry"] for feature in shape]
                
                cluster_id = file[:-4]
#                print(cluster_id)
                if "buff" in file:
                    geoms_buffer[cluster_id]=geoms 
                else:    
                    geoms_fgc[cluster_id]=geoms
                 
    return geoms_fgc, geoms_buffer

def merge_files_v2(f1, f2, out):
    fgc_shp = ogr.Open(f1)
    lyr1 = fgc_shp.GetLayer() 
#        
    fgc_shp2 = ogr.Open(f2)
    lyr2 = fgc_shp2.GetLayer()
   
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)

    merge_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    
    
    lyr1.Union(lyr2, merge_lyr)

    
    
def merge_files(f1, f2, out):
    print("Merging files...")
    try:        
        fgc_shp = ogr.Open(f1)
        lyr1 = fgc_shp.GetLayer() 
        
        fgc_shp2 = ogr.Open(f2)
        lyr2 = fgc_shp2.GetLayer()
    
        driver=ogr.GetDriverByName('ESRI Shapefile')
        ds=driver.CreateDataSource(out)
    
        merge_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    #   diff_lyr = createFieldsFrom(lyr2, diff_lyr)
    
        print("    First file uninon..")
        union1 = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat1 in lyr1:
            geom1 = feat1.GetGeometryRef()
            if geom1 != None: 
                union1 = union1.Union(geom1)
                 
            geom1 = None
        print("    Second file uninon..")    
        union2 = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat2 in lyr2:
            geom2 = feat2.GetGeometryRef()
            if geom2 != None:        
                union2 = union2.Union(geom2) 
            geom2 = None
          
                
        union1 = union1.Buffer(0.0001)
        union2 = union2.Buffer(0.0001)
        
        print("    Final uninon..")
        merge = union1.Union(union2)
        
        new_feat = ogr.Feature(merge_lyr.GetLayerDefn())
        new_feat.SetGeometry(merge)
        merge_lyr.CreateFeature(new_feat)   
    
    except:
        print("exception thrown!")
        traceback.print_exc()
        

    new_feat= None
    union1 = None   
    union2 = None            
    diff_lyr = None
    fgc_shp = None
    fgc_shp2 = None
    ds = None
    lyr1 = None
    lyr2 = None
    
    
    
    
def intersect_v2(f1,f2,out):

    fgc_shp = ogr.Open(f1)
    lyr1 = fgc_shp.GetLayer() 
#        
    fgc_shp2 = ogr.Open(f2)
    lyr2 = fgc_shp2.GetLayer()
    print(lyr1.GetFeatureCount())
    print(lyr2.GetFeatureCount())
   
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)

    diff_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    
    
    lyr1.Intersection(lyr2, diff_lyr)
#   diff_lyr = createFieldsFrom(lyr2, diff_lyr)
#    union1 = ogr.Geometry(ogr.wkbMultiPolygon)

    
    
def intersect(f1,f2, out):
#    try: 

#        print("intersection between:")
#        print("   ",f1)
#        print("   ",f2)
        fgc_shp = ogr.Open(f1)
        lyr1 = fgc_shp.GetLayer() 
#        
        fgc_shp2 = ogr.Open(f2)
        lyr2 = fgc_shp2.GetLayer()
        print(lyr1.GetFeatureCount())
        print(lyr2.GetFeatureCount())
       
        driver=ogr.GetDriverByName('ESRI Shapefile')
        ds=driver.CreateDataSource(out)
    
        diff_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    #   diff_lyr = createFieldsFrom(lyr2, diff_lyr)
        union1 = ogr.Geometry(ogr.wkbMultiPolygon)
        
    
        for feat1 in lyr1:
            geom1 = feat1.GetGeometryRef()
            if geom1 != None: 
                union1 = union1.Union(geom1)
                     
            geom1 = None
            
        union2 = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat2 in lyr2:
            geom2 = feat2.GetGeometryRef()
            if geom2 != None:        
                union2 = union2.Union(geom2) 
            geom2 = None
          
                
        union1 = union1.Buffer(0)
        union2 = union2.Buffer(0)
        
        print(union1.GetGeometryCount())
        
        diff = union1.Intersection(union2)
        
        new_feat = ogr.Feature(diff_lyr.GetLayerDefn())
        new_feat.SetGeometry(diff)
        diff_lyr.CreateFeature(new_feat)   

        new_feat= None
        union1 = None   
        union2 = None            
        diff_lyr = None
        fgc_shp = None
        fgc_shp2 = None
        ds = None
        lyr1 = None
        lyr2 = None
    
    


def difference_v2(f1,f2,out):

    fgc_shp = ogr.Open(f1)
    lyr1 = fgc_shp.GetLayer() 
#        
    fgc_shp2 = ogr.Open(f2)
    lyr2 = fgc_shp2.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)

    diff_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    
    
    lyr1.SymDifference(lyr2, diff_lyr)
    
    
    diff_lyr = None
    
    
    
def difference(f1, f2, out):
    try: 
        print("Difference between:")
        print("   ",f1)
        print("   ",f2)
        fgc_shp = ogr.Open(f1)
        lyr1 = fgc_shp.GetLayer() 
        
        fgc_shp2 = ogr.Open(f2)
        lyr2 = fgc_shp2.GetLayer()
    
        driver=ogr.GetDriverByName('ESRI Shapefile')
        ds=driver.CreateDataSource(out)
    
        diff_lyr = ds.CreateLayer('temp', lyr2.GetSpatialRef(), ogr.wkbMultiPolygon )
    #   diff_lyr = createFieldsFrom(lyr2, diff_lyr)
    
        
        union1 = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat1 in lyr1:
            geom1 = feat1.GetGeometryRef()
            if geom1 != None: 
                union1 = union1.Union(geom1)
                 
            geom1 = None
            
        union2 = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat2 in lyr2:
            geom2 = feat2.GetGeometryRef()
            if geom2 != None:        
                union2 = union2.Union(geom2) 
            geom2 = None
          
                
        union1 = union1.Buffer(0)
        union2 = union2.Buffer(0)
        
        print(union1.GetGeometryCount())
        
        diff = union1.Difference(union2)
        
        new_feat = ogr.Feature(diff_lyr.GetLayerDefn())
        new_feat.SetGeometry(diff)
        diff_lyr.CreateFeature(new_feat)   
    
    except:
        print("exception thrown!")
        traceback.print_exc()
        

    new_feat= None
    union1 = None   
    union2 = None            
    diff_lyr = None
    fgc_shp = None
    fgc_shp2 = None
    ds = None
    lyr1 = None
    lyr2 = None
    
    
def list_fields(lyr):
    lyrDefn = lyr.GetLayerDefn()
    #Lista os fields do shapefile e o respetivo tipo
    for i in range( lyrDefn.GetFieldCount() ):
        fieldName =  lyrDefn.GetFieldDefn(i).GetName()
        fieldTypeCode = lyrDefn.GetFieldDefn(i).GetType()
        fieldType = lyrDefn.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
        fieldWidth = lyrDefn.GetFieldDefn(i).GetWidth()
        GetPrecision = lyrDefn.GetFieldDefn(i).GetPrecision()
        print (fieldName + " - " + fieldType+ " " + str(fieldWidth) + " " + str(GetPrecision))

def convex_hull(file,out):
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    
    ds1=driver.CreateDataSource(out)
    convex_lyr=ds1.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    convex_lyr = createFieldsFrom(lyr,convex_lyr)
    
    # Collect all Geometry
    geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
    for feature in lyr:
        geomcol.AddGeometry(feature.GetGeometryRef())

    # Calculate convex hull
    convexhull = geomcol.ConvexHull()
    
    new_feat = ogr.Feature(convex_lyr.GetLayerDefn())
    new_feat.SetGeometry(convexhull)
    convex_lyr.CreateFeature(new_feat)      
    
    new_feat = None
    lyr = None
    ds1 = None
    convex_lyr = None
    
         
#cria um geometria concava a partir de um cunjunto de polygnos    
def test_concav(file,out):
    
    points_by_id = get_points_from_geomety(file)
    
    
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    
    ds1=driver.CreateDataSource(out)
    concave_lyr=ds1.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    concave_lyr = createFieldsFrom(lyr,concave_lyr)
    print("starting concave")


    ps = np.array([ ( points_by_id.iloc[xi,0] , np.array(points_by_id.iloc[xi,1]) ) for xi in range(0,points_by_id.shape[0]) ], dtype=object)
#    print(ps)
#    res = [temp(x)  for x in ps]
    #Processar em várias threads
    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() )
    res = pool.map( temp,ps ) 
   
    print("End of concave calculations.")
    
    
    print("Saving to file")
    res = np.asarray(res,dtype=object)


    for i in range(0, res.shape[0]):
        try:
            fgc_id = res[i][0]
            hull = res[i][1]
            
            new_ring = ogr.Geometry(ogr.wkbLinearRing)
            hull = np.asarray(hull)

            for i in range(0, hull.shape[0]):
                new_ring.AddPoint(hull[i,0], hull[i,1])
                        
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(new_ring)
            new_feat = ogr.Feature(concave_lyr.GetLayerDefn())
            new_feat.SetField("ID_R_FGC",fgc_id)
            new_feat.SetGeometry(poly)
            concave_lyr.CreateFeature(new_feat)                   
            new_feat = None
            
        except:
            print("exception thrown!")
            traceback.print_exc()
            break
    
    
def temp(fgc):
    points = []
    fgc_id = fgc[0]
    all_points = fgc[1] 
    
    if(all_points.shape[0]>2):
        points = ConcaveHull.concaveHull(all_points, 3)
        
    return (fgc_id,points)
#    q.put(points)

#Obtem os pontos que fazem parte das fronteiras das geometrias. (Apenas funciona com os ficheiros orginais das faixas, ou seja, sem merge ou union..)
def get_points_from_geomety(file):
    print("Getting geometry points")    
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    points_by_id = pd.DataFrame(data=[], columns=["id","points"])
    
    for feature in  lyr:
#        print("entrou")
        i_desc_fgc = feature.GetFieldIndex("ID_R_FGC")
        fgc_id = feature.GetFieldAsInteger(i_desc_fgc)
        geom = feature.GetGeometryRef() 
           
#        if fgc_type == 360 :
#                print("360 existe!",geom.GetGeometryCount() )
#        print("id= ", fgc_id ,"   GeometryCount= ",geom.GetGeometryCount() )
        for j in range(0,geom.GetGeometryCount() ):
            ring = geom.GetGeometryRef(j)
            if ring != None:
#                print(ring.GetPoints())
                #Se a faixa ainda nao foi vista adiciona ao dataframe
                if(points_by_id[points_by_id.iloc[:,0]==fgc_id].shape[0] == 0):
                    points_by_id = points_by_id.append({'id' : fgc_id , 'points' :[] } , ignore_index=True)
                
                #Adicionar os pontos a lista de pontos da respetiva faixa
#                temp_points = points_by_id[points_by_id.iloc[:,0]==fgc_id].iloc[0,1]
#                print("  ",j," has", ring.GetPointCount(), " points" )
                for i in range(0, ring.GetPointCount()):
                    lon, lat, z = ring.GetPoint(i)
                    points_by_id[points_by_id.iloc[:,0]==fgc_id].iloc[0,1].append((lon,lat))
#                    temp_points.append((lon,lat))
#            else:
#                print("  ",j," is None" )
#                points_by_id[points_by_id.iloc[:,0]==fgc_id].iloc[0,1] =temp_points
       
    fgc_shp = None
    lyr = None    
    
    return points_by_id



#Remove os polignos que tem uma area inferior a "area"
def remove_polygon_by_area(f1, area, out):
    fgc_shp = ogr.Open(f1)
    lyr = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)
    
    holes_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbMultiPolygon )        
    
    
    for feature in lyr:
        geom = feature.GetGeometryRef() 
        print(geom.GetGeometryCount())
        for j in range(0,geom.GetGeometryCount() ):
            ring = geom.GetGeometryRef(j)
            ring_area = ring.GetArea()
            if ring_area > area:
                new_feat = ogr.Feature(holes_lyr.GetLayerDefn())
                new_feat.SetGeometry(ring.Buffer(0))
                holes_lyr.CreateFeature(new_feat)
                new_feat = None
           
    holes_lyr = None
    ds=None
    lyr =None
    fgc_shp = None

#Remove o maior polygon    
def remove_biggest_polygon(f1, area, out):
    print("Removing biggest polygon")
    
    fgc_shp = ogr.Open(f1)
    lyr = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)
    
    holes_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbMultiPolygon )        
    
    
    max_area = 0.0
    max_index = 0
    for feature in lyr:
        geom = feature.GetGeometryRef() 
#        print(geom.GetGeometryCount())
        for j in range(0,geom.GetGeometryCount() ):
            ring = geom.GetGeometryRef(j)
            area = ring.GetArea()
#            print(area)
            if area> max_area:
                max_index = j
                max_area = area
                
              
    lyr =None
    fgc_shp = None 
#    print("max= ", max_index)
    
    fgc_shp2 = ogr.Open(f1)
    lyr2 = fgc_shp2.GetLayer()
    
    for feature in lyr2:
        geom = feature.GetGeometryRef() 
        
        for j in range(0,geom.GetGeometryCount()):
            ring = geom.GetGeometryRef(j)
            if j != max_index:
                new_feat = ogr.Feature(holes_lyr.GetLayerDefn())
                new_feat.SetGeometry(ring.Buffer(0))
                holes_lyr.CreateFeature(new_feat)
                new_feat = None
#                print(ring.GetArea())
        
    holes_lyr = None
    ds=None
    lyr =None
    fgc_shp = None

#cria um polygno do tamanho da extensão do ficheiro
def extent_polygon(file,out):
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    extent  = lyr.GetExtent()
    
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(extent[0], extent[2])
    ring.AddPoint(extent[1], extent[2])
    ring.AddPoint(extent[1], extent[3])
    ring.AddPoint(extent[0], extent[3])
    ring.AddPoint(extent[0], extent[2])
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)

    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)    
    extent_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbMultiPolygon )      
    new_feat = ogr.Feature(extent_lyr.GetLayerDefn())
    new_feat.SetGeometry(poly)
    extent_lyr.CreateFeature(new_feat)
    
    extent_lyr = None
    ds = None
    driver = None
    new_feat = None
    lyr = None
    fgc_shp = None

#
def cut_shp_file(file, ref, out):
    extent_file =  ref[:-4]+"_extent.shp"
    extent_polygon(ref,extent_file)
    
    fgc_shp = ogr.Open(extent_file)
    lyr_ext = fgc_shp.GetLayer()
    
    fgc_shp2 = ogr.Open(file)
    lyr = fgc_shp2.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)    
    final_lyr = ds.CreateLayer('temp', lyr_ext.GetSpatialRef(), ogr.wkbMultiLineString ) 
    
    lyr.Clip(lyr_ext, final_lyr)
    

    final_lyr = None
    lyr_ext = None
    lyr = None
    ds = None     


def fill_holes(file, out):
    print("Filling holes...")
    dir_path = os.path.dirname(os.path.realpath(file))
    file_name = os.path.basename(file)

    extent_polygon(file, dir_path +"/temp_extent_"+file_name)

    difference(dir_path +"/temp_extent_"+file_name, file, dir_path +"/temp_diff_extent_"+file_name )
    
    remove_biggest_polygon(dir_path +"/temp_diff_extent_"+file_name, 1,dir_path +"/temp_holes_"+file_name)

    merge_files(file, dir_path +"/temp_holes_"+file_name, out)
    


def reproject_file(file, proj, out):
    print("reprojecting: ",file)
    driver = ogr.GetDriverByName('ESRI Shapefile')
    inDataSet = driver.Open(file)
    lyr = inDataSet.GetLayer()
    
    # input SpatialReference
    inSpatialRef = lyr.GetSpatialRef()

    # output SpatialReference
    outSpatialRef = osr.SpatialReference(proj)

    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
 
    outDataSet = driver.CreateDataSource(out)
    outLayer = outDataSet.CreateLayer("temp", osr.SpatialReference(proj), geom_type=lyr.GetGeomType())

    # add fields
    inLayerDefn = lyr.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        outLayer.CreateField(fieldDefn)

    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()

    for feature in lyr:
        
        geom = feature.GetGeometryRef()
        # reproject the geometry
        if geom == None:
            break
        geom.Transform(coordTrans)
      
        outFeature = ogr.Feature(outLayerDefn)

        outFeature.SetGeometry(geom)
        for i in range(0, outLayerDefn.GetFieldCount()):
            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), feature.GetField(i))

        outLayer.CreateFeature(outFeature)
        outFeature = None

    inDataSet = None
    outDataSet = None
    
def raster_to_points(raster, out, fgc_type):

    r = gdal.Open(raster)
    prj=r.GetProjection()
#    proj = 'PROJCS["ETRS89_Portugal_TM06",GEOGCS["GCS_ETRS_1989",DATUM["ETRS_1989", SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0], UNIT["Degree",0.017453292519943295]], PROJECTION["Transverse_Mercator"], PARAMETER["latitude_of_origin",39.66825833333333],PARAMETER["central_meridian",-8.133108333333334],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
#    
    band = r.GetRasterBand(1) 
    a = band.ReadAsArray().astype(np.float)
    (y_index, x_index) = np.nonzero(a > 0.0)

    (upper_left_x, x_size, x_rotation, upper_left_y, y_rotation, y_size) = r.GetGeoTransform()
    x_coords = x_index * x_size + upper_left_x + (x_size / 2) 
    y_coords = y_index * y_size + upper_left_y + (y_size / 2) 
    

    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)    
    points_lyr = ds.CreateLayer('temp', osr.SpatialReference(prj), geom_type=ogr.wkbPoint )      
    
    newfield = ogr.FieldDefn("VALUE", ogr.OFTReal)
    points_lyr.CreateField(newfield)
    
    if fgc_type != None:
        newfield = ogr.FieldDefn("TYPE", ogr.OFTString)
        points_lyr.CreateField(newfield)
    
    for i in range(0,x_coords.shape[0]):
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(x_coords[i], y_coords[i])
        
        new_feat = ogr.Feature(points_lyr.GetLayerDefn())
        new_feat.SetGeometry(point)
        if fgc_type != None:
            new_feat.SetField("TYPE", fgc_type )
        
        new_feat.SetField("VALUE", a[ y_index[i], x_index[i] ] )
        points_lyr.CreateFeature(new_feat)
        new_feat = None
    
    
    points_lyr = None
    driver =None

def get_geometries_count(file):
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    count = lyr.GetFeatureCount()
    print(count)
    return count
    

def merge_points_files(f1,f2,out):
    fgc_shp = ogr.Open(f1)
    lyr = fgc_shp.GetLayer()
    
    proj = 'PROJCS["ETRS89_Portugal_TM06",GEOGCS["GCS_ETRS_1989",DATUM["ETRS_1989", SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0], UNIT["Degree",0.017453292519943295]], PROJECTION["Transverse_Mercator"], PARAMETER["latitude_of_origin",39.66825833333333],PARAMETER["central_meridian",-8.133108333333334],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
    
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)    
    points_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), geom_type=ogr.wkbPoint )    
    
    newfield = ogr.FieldDefn("TYPE", ogr.OFTString)
    points_lyr.CreateField(newfield)
    
    for feature in lyr:
        geom = feature.GetGeometryRef()
        i_type = feature.GetFieldIndex("TYPE")
        f_type = feature.GetFieldAsString(i_type )
        
        new_feat = ogr.Feature(points_lyr.GetLayerDefn())
        new_feat.SetGeometry(geom)
        new_feat.SetField("TYPE", f_type )
        points_lyr.CreateFeature(new_feat)
        new_feat = None
    
    fgc_shp = ogr.Open(f2)
    lyr = fgc_shp.GetLayer()
        
    for feature in lyr:
        geom = feature.GetGeometryRef()
        i_type = feature.GetFieldIndex("TYPE")
        f_type = feature.GetFieldAsString(i_type )
        
        new_feat = ogr.Feature(points_lyr.GetLayerDefn())
        new_feat.SetGeometry(geom)
        new_feat.SetField("TYPE", f_type )
        points_lyr.CreateFeature(new_feat)
        new_feat = None  
        
        
    ds = None
    points_lyr = None      
    
    

if __name__ == '__main__':    
    
    start = timer()
    
    
#    cut_shp_file("C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/estradas_portugal/estradas.shp","C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/FGC/FGC_1419.shp", "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/FGC/test_estradas_open.shp")
    
    
#    merge_files("../../temp/habitacoes/temp_1_concave_habi_FGC_1413.shp", "../../temp/habitacoes/habi_FGC_1413.shp" , "../../temp/habitacoes/testes.shp")
    
#    points_by_id = get_points_from_geomety("C:/Users/rafon/OneDrive/Ambiente de Trabalho/exp/correct/hab/1_concave_habi_FGC_1413.shp")
##   
    
#    remove_polygon_by_area("../Cardigos/FGC/estradas/cleaned_buffer_10m_merged.shp", 200, "../../exp/series/cleaned_buffer_10m_merged.shp")  
#    cut("../../nova/S2A_MSIL1C_20160430T112122_N0201_R037_T29SND_20160430T112639_/adjusted_B04.tiff", "../../nova/novo/FGC_1418_merged.shp", "../../nova/novo/FGC_1418_cut.tiff")
#    raster_to_points("../../nova/novo/FGC_1418_cut.tiff","../../nova/novo/points_merged.shp")
    
#    
#    clusters_file = "../../exp/series/clusters/data/temp/clusters_polygons_final.shp"
#    buffer_file = "../Cardigos/FGC/estradas/cleaned_buffer_20m_merged.shp"
#    fgc_file = "../Cardigos/FGC/estradas/merged.shp" 
#    
#    intersect_v2(clusters_file, buffer_file,"../../exp/series/clusters/data/temp/temp_intersection_buffer.shp" )
#    intersect_v2(clusters_file, fgc_file,"../../exp/series/clusters/data/temp/temp_intersection_fgc.shp" )


#    raster = "../Imagem de referencia/S2A_MSIL2A_20190214T112151_N0211_R037_T29SND_20190214T121343.SAFE/GRANULE/L2A_T29SND_A019053_20190214T112801/IMG_DATA/R10m/T29SND_20190214T112151_B08_10m.tif"
 
#    inDataSet = gdal.Open(raster)
#    proj = inDataSet.GetProjectionRef()
#    print(proj)
#    reproject_file("../../exp/series/clusters/data/temp/temp_intersection_fgc.shp",proj ,"../../exp/series/clusters/data/temp/intersection_fgc_polygons.shp")

    
#    merge_files("../../exp/series/cleaned_buffer_10m_merged.shp","../../exp/series/cleaned_buffer_20m_merged.shp", "../../exp/series/temp_merge.shp" )
#    merge_files("../../exp/series/temp_merge.shp","../../exp/series/merged_estradas.shp", "../../exp/series/buffers_unidos.shp" )


#    test_concav("../../exp/series/cluster-5113.shp", "../../exp/series/cluster_5113_polygon.shp")
    
    
#    convex_hull("../../exp/series/clusters/cluster-5113.shp", "../../exp/series/clusters/convex_cluster_5113.shp")
    
#    intersect("../../exp/series/clusters/polygon_cluster_5113.shp","../../exp/series/cleaned_buffer_20m_merged.shp","../../exp/series/clusters/buff_20m_cluster_5113.shp")
#    intersect("../../exp/series/clusters/polygon_cluster_2225.shp","../../exp/series/cleaned_buffer_20m_merged.shp","../../exp/series/clusters/buff_20m_cluster_2225.shp")


#    test_concav("../../exp/correct/hab/habi_FGC_1413.shp", "../../exp/correct/hab/1_concave_habi_FGC_1413.shp")
#    test_concav("../../exp/correct/hab/1_concave_habi_FGC_1413.shp","../../exp/correct/hab/2_concave_habi_FGC_1413.shp")
    
    proj = 'PROJCS["ETRS89_Portugal_TM06",GEOGCS["GCS_ETRS_1989",DATUM["ETRS_1989", SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0], UNIT["Degree",0.017453292519943295]], PROJECTION["Transverse_Mercator"], PARAMETER["latitude_of_origin",39.66825833333333],PARAMETER["central_meridian",-8.133108333333334],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
#  
#    reproject_file("C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/estradas_portugal/estradas.shp",proj, "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/estradas_portugal/estradas_reprojected.shp")
#    
    duration = timer() - start
    print("Duration: ",duration)
    
#    start = timer()
#    merge_files("../../exp/correct/hab/2_concave_habi_FGC_1413.shp","../../exp/correct/hab/habi_FGC_1413.shp", "../../exp/correct/hab/merged_test_habi_FGC_1413.shp" )
    
#    fill_holes("../../exp/correct/hab/merged_test_habi_FGC_1413.shp", "../../exp/correct/hab/merged_test_habi_FGC_1413.shp")
#    duration = timer() - start
#    print("Duration: ",duration)
















