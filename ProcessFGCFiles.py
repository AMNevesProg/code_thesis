# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 16:18:41 2019

IMPORTANT: This script needs to be executed in the command line due to some problems with the parallelization

@author: Ricardo
"""

import os,sys
import traceback
import Geometries_FGC
import CreateSections
from osgeo import gdal, ogr, osr
import numpy as np
from timeit import default_timer as timer


def extract_openstreemaps_roads(fgc_dir,fgc_file):
    
    
#    portugal_roads ="C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/estradas_portugal/estradas.shp"
    portugal_roads = fgc_dir+"/../openstreetmap_estradas/estradas.shp"
    
    Geometries_FGC.cut_shp_file(portugal_roads,fgc_file, fgc_dir+"/temp_openstreetmap.shp")
    Geometries_FGC.addBuffer(fgc_dir+"/temp_openstreetmap.shp",10, fgc_dir+"/openstreetmap_roads_merged.shp")
    Geometries_FGC.merge_geometries_by_field(fgc_dir+"/openstreetmap_roads_merged.shp", fgc_dir+"/openstreetmap_roads.shp", None)
    
    driver = ogr.GetDriverByName("ESRI Shapefile")
    driver.DeleteDataSource(fgc_dir+"/temp_openstreetmap.shp")
    driver.DeleteDataSource(fgc_dir+"/openstreetmap_roads_merged.shp")

    
    
#Separa shapefile em varios ficheiros com diferentes tipos de faixas
def split_fgc(file):
    
    dir_path = os.path.dirname(os.path.realpath(file))
    print(dir_path)
    
    try:
        os.mkdir(dir_path+"/estradas")
        os.mkdir(dir_path+"/habitacoes")
        os.mkdir(dir_path+"/linhas")
        os.mkdir(dir_path+"/redeprimaria")
    except:
        print("Erro ao criar pastas")
        traceback.print_exc()
    
    
    fgc_shp = ogr.Open(file)
    lyr = fgc_shp.GetLayer()
    
    Geometries_FGC.list_fields(lyr)
    

    #criar novos ficheiros com as faixas separadas por tipo de faixa
    driver=ogr.GetDriverByName('ESRI Shapefile')
    
    ds1=driver.CreateDataSource(dir_path +"/habitacoes/habi_FGC.shp")
    round_lyr=ds1.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    
    ds2=driver.CreateDataSource(dir_path +"/linhas/linhas_FGC.shp")
    lines_lyr=ds2.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    
    ds3=driver.CreateDataSource(dir_path+"/estradas/estradas_FGC.shp")
    double_lyr=ds3.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    
    ds4=driver.CreateDataSource(dir_path+"/redeprimaria/RP_FGC.shp")
    rp_lyr=ds4.CreateLayer('temp', lyr.GetSpatialRef(), ogr.wkbPolygon)
    
    
    #cria campos da layer do shapefile no novo ficheiro
    round_lyr = Geometries_FGC.createFieldsFrom(lyr, round_lyr)
    lines_lyr =  Geometries_FGC.createFieldsFrom(lyr, lines_lyr)
    double_lyr =  Geometries_FGC.createFieldsFrom(lyr, double_lyr)
    rp_lyr =  Geometries_FGC.createFieldsFrom(lyr, rp_lyr)
    
    
    exists_round= exists_lines = exists_double = exists_rp = False
    
    for feature in  lyr:
#        i_inter_2017 = feature.GetFieldIndex("INTER_2017")
        i_desc_fgc = feature.GetFieldIndex("DESC_FGC")
        fgc_type = feature.GetFieldAsInteger(i_desc_fgc)
        
        geom = feature.GetGeometryRef()
        
        if geom != None:
             #fgc_type codes are in the file "Plano Municipal de defesa da floresta contra incêndios" page 53
            if fgc_type == 1  or fgc_type == 2 or fgc_type == 3:
                exists_round = True
                new_feat = ogr.Feature(round_lyr.GetLayerDefn())
                new_feat.SetFrom(feature)
                new_feat.SetGeometry(geom.Buffer(0))
                round_lyr.CreateFeature(new_feat)                   
                new_feat = None
                
            if fgc_type == 13  or fgc_type == 10 or fgc_type == 7 or fgc_type == 6 :
                exists_lines = True
                new_feat = ogr.Feature(lines_lyr.GetLayerDefn())
                new_feat.SetFrom(feature)
                new_feat.SetGeometry(geom.Buffer(0))
                lines_lyr.CreateFeature(new_feat)                   
                new_feat = None
                
            if fgc_type == 5 or fgc_type == 4:
                exists_double = True
                new_feat = ogr.Feature(double_lyr.GetLayerDefn())
                new_feat.SetFrom(feature)
                new_feat.SetGeometry(geom.Buffer(0))
                double_lyr.CreateFeature(new_feat)                   
                new_feat = None
        
            if fgc_type == 8:
                exists_rp = True
                new_feat = ogr.Feature(rp_lyr.GetLayerDefn())
                new_feat.SetFrom(feature)
                new_feat.SetGeometry(geom.Buffer(0))
                rp_lyr.CreateFeature(new_feat)                   
                new_feat = None
        
#        print( feature.GetFieldAsString(index) )
        
    double_lyr= None
    lines_lyr= None
    round_lyr= None
    rp_lyr= None
    lyr = None 
    ds1 = None
    ds2 = None
    ds3 = None
    ds4 = None

    if not exists_round:
        os.remove(dir_path +"/habitacoes/habi_FGC.shp")
    if not exists_lines:
        os.remove(dir_path +"/linhas/linhas_FGC.shp")
    if not exists_double:
        os.remove(dir_path+"/estradas/estradas_FGC.shp")
    if not exists_rp:
        os.remove(dir_path+"/redeprimaria/RP_FGC.shp")

 

def merge_files(path):
    print("Merging geometries...")
    for subdir, dirs, files in os.walk(path):
        for file in files:
            
            file_path = subdir+'/'+file
            
            
            if file.endswith(".shp") and (file.startswith("habi") or file.startswith("linhas") or file.startswith("estradas") or file.startswith("RP")) :
                new_file = subdir+'/merged.shp'
                temp_file = subdir+'/temp_'+file
                
                #Junta geometrias
                start = timer()
                Geometries_FGC.merge_geometries_by_field(file_path, temp_file, None)
                
                #Buffer para remover falhas entre polignos
                Geometries_FGC.addBuffer(temp_file,0.0001,new_file)
                os.remove(temp_file)
                end = timer()
                print("duration merge:", (end-start))
                
                if file.startswith("habi"):
                    #calcular a concav hull
                    
                    print("A calcular o interior das faixas ao redor das habitações...")
                    start = timer()
                    Geometries_FGC.test_concav(file_path, subdir+"/temp_1_concave_"+file)
                    Geometries_FGC.test_concav( subdir+"/temp_1_concave_"+file, subdir+"/temp_2_concave_"+file)
                    Geometries_FGC.merge_files(subdir+"/temp_2_concave_"+file, new_file, subdir+"/temp_interior_"+file )
                    Geometries_FGC.fill_holes(subdir+"/temp_interior_"+file, subdir+"/interior_habi.shp")
                    end = timer()
                    print("duration Interior habi:", (end-start))
                
               

def create_buffers(path):
    print("Creating buffers...")
    for subdir, dirs, files in os.walk(path):
        for file in files:
            
            file_path = subdir+'/'+file
    
            if file.endswith(".shp") and file.startswith("merged") :
              
                #Criação de buffers exteriores de diferentes tamanhos
#                Geometries_FGC.addBuffer(file_path,10,subdir+'/buffer_10m_'+file)
                Geometries_FGC.addBuffer(file_path,20,subdir+'/buffer_20m_'+file)
          
    
    
#reomve as zonas que pertencem a outras faixas, de forma a não fazerem parte dos buffers exteriores  
def remove_overlapping_areas(path):
    print("Removing overlapping areas...")
    estradas_inside = path+"/openstreetmap_roads.shp"
    estradas_merged = path+"/estradas/merged.shp"
    habi_interior = path+"/habitacoes/interior_habi.shp"
    linhas_merged = path+"/linhas/merged.shp"
    rp_merged = path+"/redeprimaria/merged.shp"
    for subdir, dirs, files in os.walk(path):
        for file in files:
            
            file_path = subdir+'/'+file
            
            if file.endswith(".shp") and file.startswith("buffer"):
                Geometries_FGC.difference(file_path, estradas_merged, subdir+'/temp_dif_estradas.shp')
                Geometries_FGC.difference(subdir+'/temp_dif_estradas.shp', estradas_inside, subdir+'/temp_dif_estradas1.shp')
                Geometries_FGC.difference(subdir+'/temp_dif_estradas1.shp', habi_interior, subdir+'/temp_dif_habi.shp')
                Geometries_FGC.difference(subdir+'/temp_dif_habi.shp', linhas_merged, subdir+'/temp_dif_linhas.shp')
                if os.path.exists(rp_merged):
                    Geometries_FGC.difference(subdir+'/temp_dif_linhas.shp', rp_merged, subdir+'/temp_cleaned_'+file)
                else:
                    Geometries_FGC.difference(subdir+'/temp_dif_habi.shp', linhas_merged, subdir+'/temp_cleaned_'+file)


def remove_fragements(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            
            file_path = subdir+'/'+file
            if "temp_cleaned_buffer_" in file  and file.endswith(".shp"):
                Geometries_FGC.remove_polygon_by_area(file_path, 200, subdir+'/'+file.replace("cleaned","temp"))
                Geometries_FGC.merge_geometries_by_field(subdir+'/'+file.replace("cleaned","temp"), subdir+'/'+file.replace("temp_cleaned","cleaned"), None)
                
    
    
def create_sections(file_dir):
     
    CreateSections.extract_points_fgc_and_buffer(file_dir, "estradas")
    CreateSections.create_clusters(file_dir, "estradas")
    CreateSections.create_clusters_polygons(file_dir+"/estradas/points_inout_clusters.shp", file_dir+"/estradas/temp_polygons.shp")

    CreateSections.extract_points_fgc_and_buffer(file_dir, "linhas")
    CreateSections.create_clusters(file_dir,"linhas")
    CreateSections.create_clusters_polygons(file_dir+"/linhas/points_inout_clusters.shp", file_dir+"/linhas/temp_polygons.shp")

    CreateSections.extract_points_fgc_and_buffer(file_dir, "habitacoes")
    CreateSections.create_clusters(file_dir,"habitacoes")
    CreateSections.create_clusters_polygons(file_dir+"/habitacoes/points_inout_clusters.shp", file_dir+"/habitacoes/temp_polygons.shp")


def cut_fgc_and_buffer_polygons(file_dir,fgc_type):

    clusters_file = file_dir+"/"+fgc_type+"/polygons.shp"
    buffer_file = file_dir+"/"+fgc_type+"/cleaned_buffer_20m_merged.shp"
    fgc_file = file_dir+"/"+fgc_type+"/merged.shp"
    
    clusters_shp = ogr.Open(clusters_file)
    clusters = clusters_shp.GetLayer()
    
    buffer_shp = ogr.Open(buffer_file)
    buffer = buffer_shp.GetLayer()
    buffer_feat = buffer.GetFeature(0)
    buffer_geom = buffer_feat.GetGeometryRef()
    
    fgc_shp = ogr.Open(fgc_file)
    fgc = fgc_shp.GetLayer()
    fgc_feat = fgc.GetFeature(0)
    fgc_geom = fgc_feat.GetGeometryRef()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')

    try:
        os.mkdir(file_dir+"/"+fgc_type+"/clusters_data")
    except:
        print("Erro ao criar pasta")
        traceback.print_exc()
    
    for feature in clusters:
        geom = feature.GetGeometryRef()
        i_cluster_id = feature.GetFieldIndex("CLUSTER_ID")
        cluster_id = int(float(feature.GetFieldAsString(i_cluster_id)   ))
#        
                
        buff = buffer_geom.Intersection(geom)
        fgc_in = fgc_geom.Intersection(geom)
            
        ds=driver.CreateDataSource( file_dir+"/"+fgc_type+"/clusters_data/buff_c"+str(cluster_id)+".shp")    
        buff_lyr = ds.CreateLayer('temp', clusters.GetSpatialRef(), geom_type=ogr.wkbMultiPolygon )
                
        ds2=driver.CreateDataSource(file_dir+"/"+fgc_type+"/clusters_data/fgc_c"+str(cluster_id)+".shp")    
        fgc_lyr = ds2.CreateLayer('temp', clusters.GetSpatialRef(), geom_type=ogr.wkbMultiPolygon )
                
        new_feat = ogr.Feature(buff_lyr.GetLayerDefn())
        new_feat.SetGeometry(buff.Buffer(0))
        buff_lyr.CreateFeature(new_feat)
        new_feat = None
                
        new_feat = ogr.Feature(fgc_lyr.GetLayerDefn())
        new_feat.SetGeometry(fgc_in.Buffer(0))
        fgc_lyr.CreateFeature(new_feat)
        new_feat = None
                

def cut_fgc_and_buffer_polygons_forall(file_dir):
    cut_fgc_and_buffer_polygons(file_dir,"estradas")
    cut_fgc_and_buffer_polygons(file_dir,"linhas")
    cut_fgc_and_buffer_polygons(file_dir,"habitacoes")
          
                
def delete_temporary_files(path):

    for subdir, dirs, files in os.walk(path):
        for file in files:
            file_path = subdir+'/'+file
            if file.startswith("temp"):
                os.remove(file_path)

    
    
def start(file):
#    file = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/fgc/FGC_1419.shp"
#    file_dir = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/fgc"
    
    file_dir = os.path.dirname(os.path.realpath(file))

    start = timer()
    split_fgc(file)
    end = timer()
    print("duration split: ",(end-start))
    
    start = timer()
    extract_openstreemaps_roads(file_dir, file)
    end = timer()
    print("OpenStreetMap roads: ",(end-start))
    
    merge_files(file_dir)
        
    start = timer()
    create_buffers(file_dir)
    end = timer()
    print("duration buffers: ",(end-start))
#    
    start = timer()
    remove_overlapping_areas(file_dir)
    end = timer()
    print("duration overlaping: ",(end-start))
#    
    start = timer()
    remove_fragements(file_dir)
    end = timer()
    print("duration fragments: ",(end-start))
#    
    
    start = timer()
    create_sections(file_dir)
    end = timer()
    print("duration create sections: ",(end-start))


    start = timer()
    cut_fgc_and_buffer_polygons_forall(file_dir)
    end = timer()
    print("duration cut fgc and buffer sections: ",(end-start))

    delete_temporary_files(file_dir) 
    
    
    print("end")
    
    
if __name__ == '__main__':
    
#    file = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/FGC/corrected_FGC_1413.shp"
#    file_dir = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/FGC"   
    
    if len(sys.argv) != 2 :
        print("Missing arguments! Command usage:\n")
        print("   python ProcessFGCFiles.py '<path_to_fgci_shapefile.shp>' ")
        sys.exit()
    
    file = sys.argv[1]
    
    file = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/fgc/FGC_1419.shp"
#    file_dir = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/fgc"
    
    start("C:/Users/rafon/OneDrive/Ambiente de Trabalho/Santarem/fgc/FGC_1419.shp")




















