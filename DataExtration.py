# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 16:03:11 2019

@author: Ricardo
"""

from osgeo import ogr, gdal
import itertools
import Geometries_FGC
import matplotlib.pyplot as plt
import os, sys
import numpy as np
import pandas as pd
import DataExtration
import math
import re
import csv
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import normalize
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import f1_score
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectFromModel
from timeit import default_timer as timer



gdal.AllRegister()
gdal.UseExceptions()
ogr.DontUseExceptions()
  

def formatFloat(value):
    return "{0:.7f}".format(value)


def get_sections(folder, kmeans):
    
    if not os.path.isfile(folder+"/polygons.shp"):
        print("There is no file with sections!")
        sys.exit()
    
    fgc_shp = ogr.Open(folder+"/polygons.shp")
    
    if kmeans:  
        fgc_shp = ogr.Open(folder+"/kmeans_seccoes_analisadas.shp") #sections que foram analisadas usando o Kmeans     
    
    lyr = fgc_shp.GetLayer()
    
    data = []
    for feat in lyr:
        geom = feat.GetGeometryRef()
        
#        i_cluster_id = feat.GetFieldIndex("OLD_ID")
#        cluster_id = feat.GetFieldAsString(i_cluster_id)
        i_type = feat.GetFieldIndex("CLUSTER_ID")
        classe = feat.GetFieldAsString(i_type)      
    
        if classe not in data:
            data.append(int(float(classe)))
      
#    print("get_sections: ",len(data))    
    return data
    
    
def get_ground_truth(sections, folder):
    
    #os dados de groundtruth foram inseridos no ficheiro polygons manualmente usanto o qgis ( ver readme da pasta Ground_Truth)
    fgc_shp = ogr.Open(folder+"/polygons.shp")
    
    lyr = fgc_shp.GetLayer()
    
    data = {}
    for feat in lyr:
        geom = feat.GetGeometryRef()
        
        i_cluster_id = feat.GetFieldIndex("CLUSTER_ID")
        if i_cluster_id == -1:
            print("There is no ground truth!")
            return {}
        
        cluster_id = int(float(feat.GetFieldAsString(i_cluster_id)  ))
        
        i_type = feat.GetFieldIndex("INTERVEN") #Field que foi criado manualmente para o caso de estudo de Mação. Usado para indicar se um secção foi intervencionada.
        
        classe = 0
        if i_type != -1:    
            classe = feat.GetFieldAsString(i_type)      
        
        if cluster_id in sections:
            data[cluster_id] = int(classe)


    return data

def rasterToArray(file):
     raster= gdal.Open(file)
     band = raster.GetRasterBand(1)
     arr = np.array(band.ReadAsArray(0,0,raster.RasterXSize,raster.RasterYSize))
     return arr[arr[:,:] != 0]
    
    
def save_to_file(file, data,col_names):
#    
    with open(file,'w',newline='') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(col_names)
        for row in data:
            csv_out.writerow(row)      

def cluster_data(fgc_dir,fgc_type):   
    path = fgc_dir+"/"+fgc_type
    
#    print("path",path)
        
    s2_data = {}
    s1_data = {}
#    to_remove = []
    for subdir, dirs, files in os.walk(path):
 
        for file in files:
            
            file_path = subdir+"/"+file
            
            if  ( "s2" in subdir or "s1" in subdir) and (file.endswith("tiff") or file.endswith("jp2") or file.endswith("img")):
                
                date = re.findall("[0-9]{8}T[0-9]*", file)[0]
                date = date[:-7]

                cluster_id = file.split("_")[2]
                
                position = ""
                other = ""
                if "buffer" in file:
                    other = file.replace("buffer_20m_c","fgc_"+fgc_type+"_c")
                    position = "out"
                else: 
                    other = file.replace("fgc_"+fgc_type+"_c","buffer_20m_c")
                    position = "in"
                    
                other = subdir +"/"+other
                
                if (date,position, cluster_id) not in s2_data and "s2" in subdir:
                    s2_data[(date,position, cluster_id)] = (date, position,cluster_id,0.0, 0.0, 0.0, 0.0, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
                    
                if (date,position, cluster_id) not in s1_data and "s1" in subdir:
                    s1_data[(date,position, cluster_id)] = (date, position,cluster_id,0.0, 0.0)
               
                value = float(np.mean(rasterToArray(file_path)))
               
                if "s2" in subdir:
                    old = s2_data[(date,position, cluster_id)]
                if "s1" in subdir:
                    old_s1 =  s1_data[(date,position, cluster_id)]
               
#               if math.isnan(value) or os.path.isfile(other) == False:
#                    to_remove.append(cluster_id)
                    
                if "NDVI" in file:
#                    print("s2",cluster_id)
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], value, old[4], old[5],old[6],old[7],old[8],old[9],old[10],old[11],old[12],old[13],old[14],old[15])
#                 
                elif "NDWI" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], value, old[5],old[6],old[7],old[8],old[9],old[10],old[11],old[12],old[13],old[14],old[15])
#                   
                elif "SAVI" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], value,old[6],old[7],old[8],old[9],old[10],old[11],old[12],old[13],old[14],old[15])

                elif "IRECI" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],value,old[7],old[8],old[9],old[10],old[11],old[12],old[13],old[14],old[15])
                elif "SR" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],value,old[8],old[9],old[10],old[11],old[12],old[13],old[14],old[15])  
                elif "B02" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],value,old[9],old[10],old[11],old[12],old[13],old[14],old[15])
                elif "B03" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],value,old[10],old[11],old[12],old[13],old[14],old[15]) 
                elif "B04" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],value,old[11],old[12],old[13],old[14],old[15]) 
                elif "B05" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],old[10],value,old[12],old[13],old[14],old[15]) 
                elif "B06" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],old[10],old[11],value,old[13],old[14],old[15]) 
                elif "B07" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],old[10],old[11],old[12],value,old[14],old[15]) 
                elif "B08" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],old[10],old[11],old[12],old[13],value,old[15]) 
                elif "B11" in file:
                    s2_data[(date,position, cluster_id)] = (old[0], old[1],old[2], old[3], old[4], old[5],old[6],old[7],old[8],old[9],old[10],old[11],old[12],old[13],old[14],value)
                elif "Beta0_VH" in file:
#                    print("s1",cluster_id)
                    s1_data[(date,position, cluster_id)] = (old_s1[0], old_s1[1],old_s1[2], value, old_s1[4])
                elif "Beta0_VV" in file:
                    s1_data[(date,position, cluster_id)] = (old_s1[0], old_s1[1],old_s1[2], old_s1[3], value)    
                   
                
                
                
#    to_remove = np.unique(np.asarray(to_remove))
    
    print("done!")
    data_arr = np.array(list(s2_data.values()))
    s2_data = pd.DataFrame(data=data_arr, columns=["date","position","cluster","ndvi","ndwi", "savi","ireci","sr", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11"])

    if len(s1_data) > 0 :
        data_arr = np.array(list(s1_data.values()))
    else: 
        data_arr = []
    s1_data = pd.DataFrame(data=data_arr, columns=["date","position","cluster", "VH", "VV"])

    return  s2_data.values, s1_data.values
#    return  [], s1_data.values


def remove_elements(y,to_remove):
    
    if "c" in str(y[0,2]):
        valid = np.isin(y[:,2], to_remove)
    else:
        valid = np.isin(y[:,2], to_remove.astype(int))
    
    y = y[~valid]
    
    return y


def get_in_out(y):
    
    clusters_o = np.unique(y[y[:,1] == "out",2])
    clusters_i = np.unique(y[y[:,1] == "in",2])
    
    y_dentro = y[y[:,1] == "in",:]
    
    #filtra os clusters que não têm buffer exterior
    valid = np.isin(y_dentro[:,2],clusters_o)
    y_dentro = y_dentro[valid]
    
    
    y_fora = y[y[:,1] == "out",:]
    valid = np.isin(y_fora[:,2],clusters_i)
    y_fora = y_fora[valid]
    
    return y_fora, y_dentro

    

def read_data(fgc_dir,fgc_type):    
    
    df1 = []
    df1=pd.read_csv(fgc_dir+"/"+fgc_type+"/dados_"+fgc_type+"_s1.csv", sep=',', engine= "python")
    df2=pd.read_csv(fgc_dir+"/"+fgc_type+"/dados_"+fgc_type+"_s2.csv", sep=',', engine= "python")
          
    count_s1_dates = float(len(np.unique(df1["date"])))
    count_s2_dates = float(len(np.unique(df2["date"])))
    
    
    #verifica se tem informação das mesmas datas dentro e fora
    counter = df2.groupby(['cluster',"position"]).count().reset_index()
    means = counter.groupby(["cluster"]).mean().reset_index()
    to_remove = np.asarray(means.loc[means["date"]<count_s2_dates]["cluster"]) # 20 datas
    
    my_data = df2.values
    
   #ve linhas que tem valores invalidos e remove os clusters
    to_remove = np.append(np.unique( my_data[np.isnan(my_data[:,3].astype(float))][:,2] ), to_remove)
    
    to_remove = np.append( np.unique(my_data[(my_data[:,3] == 0.0) & (my_data[:, 4] == 0.0) ][:,2]), to_remove)   
    s2_data = remove_elements(my_data, np.unique(to_remove))

    
    s1_data = []
    if len(df1)>0:
        counter = df1.groupby(['cluster',"position"]).count().reset_index()
        means = counter.groupby(["cluster"]).mean().reset_index()
        to_remove_2 = np.asarray(means.loc[means["date"]<count_s1_dates]["cluster"]) # 15 datas
        
        
        my_data = df1.values
        
        #ve linhas que tem valores invalidos e remove os clusters
        to_remove_2 = np.append(np.unique( my_data[np.isnan(my_data[:,3].astype(float))][:,2] ), to_remove_2)
    
        to_remove_2 = np.append(np.unique(my_data[(my_data[:,3] == 0.0) & (my_data[:, 4] == 0.0)][:,2]), to_remove_2)
        
        s1_data = remove_elements(my_data, np.unique(to_remove_2))
        s2_data = remove_elements(s2_data, np.unique(to_remove_2))
    

    return s2_data,s1_data


def manual_feature_selection(x,y, feat_names):
    
    
    features = []
    for col_name in x.columns:
        
        for feat_name in feat_names:
            
            if feat_name in col_name:
                features.append(col_name)
            
            
    return x.loc[:,features]       
    


def feature_selection(x,y):
    
    X_rn = preprocessing.normalize(x)
    clf = LassoCV(cv=5)
  
    sfm = SelectFromModel(clf)
    sfm.fit(X_rn, y)
    X_transform = sfm.transform(X_rn)
    select = sfm.get_support()
    
    print(x.shape[1])
    print(X_transform.shape[1])
    feat_scores = sfm.estimator_.coef_[select]
    feat_names = x.columns[select]
    
    final_feats = np.append(feat_names[:,None],feat_scores[:,None],axis =1 )
    df_scores = pd.DataFrame(data= final_feats,columns = ["feat_name", "score"])
    df_scores = df_scores.astype({"feat_name":str, "score":float})
    print(df_scores.nlargest(25,"score"))
    
    print("Removed:")
    print(x.columns[~select])

    

    count_keep = []
    for feat in feat_names:
        feature = feat.split("_")[-1]
        count_keep.append(("Usadas",feature,1))
    
    count_keep = pd.DataFrame(data= np.asarray(count_keep),columns = ["tipo","feat_name", "count"])
    print(count_keep.groupby(["feat_name","tipo"]).count())
    
    
    count_removed=[]
    for feat_r in x.columns[~select]: 
        feature = feat_r.split("_")[-1]
        count_removed.append(("Eliminadas",feature,1))
    
    count_removed = pd.DataFrame(data= np.asarray(count_removed) ,columns = ["tipo","feat_name", "count"])
    print(count_removed.groupby(["feat_name","tipo"]).count() )
    
    print("")
    print("")
    ch_keep = []
    for feat in feat_names:
        feature = feat.split("_")[-2]
        ch_keep.append(("Usadas",feature,1))
    
    ch_keep = pd.DataFrame(data= np.asarray(ch_keep),columns = ["tipo","feat_name", "count"])
    print(ch_keep.groupby(["feat_name","tipo"]).count())
    
    ch_removed=[]
    for feat_r in x.columns[~select]: 
        feature = feat_r.split("_")[-2]
        ch_removed.append(("Eliminadas",feature,1))
    
    ch_removed = pd.DataFrame(data= np.asarray(ch_removed) ,columns = ["tipo","feat_name", "count"])
    print(ch_removed.groupby(["feat_name","tipo"]).count())
  

    return x.loc[:,select]


def delta_diff(diff,n_dates):
    
    deltas = []
    
    for i in range(0,diff.shape[0],n_dates):
        cluster_id = diff[i,0]
        
        for j in range(i+1,i+n_dates):
            deltaj = diff[j,1:] - diff[j-1,1:] 
            deltaj= np.insert(deltaj,0,cluster_id)
            deltas.append(deltaj)
            
    return np.asarray(deltas)



def metrics_(data_s1,data_s2):

    mean_s2 = data_s2.groupby(['cluster']).mean().reset_index()
    max_s2 = data_s2.groupby(['cluster']).max().reset_index()
    min_s2 = data_s2.groupby(['cluster']).min().reset_index()
    std_s2 = data_s2.groupby(['cluster']).std().reset_index()
    
    if len(data_s1) == 0: 
        return mean_s2,max_s2,min_s2,std_s2 #''',quantile_90_s2,quantile_10_s2'''
    
       
    mean_s1 = data_s1.groupby(['cluster']).mean().reset_index()
    max_s1 = data_s1.groupby(['cluster']).max().reset_index()
    min_s1 = data_s1.groupby(['cluster']).min().reset_index()
    std_s1 = data_s1.groupby(['cluster']).std().reset_index()
        
    empty = np.zeros((mean_s2.shape[0],4))
    newdf = pd.DataFrame(data=empty, columns=["VH","VV", "ratio", "diff"])
    mean_final = pd.concat([mean_s2,newdf], axis = 1)
    max_final = pd.concat([max_s2,newdf], axis = 1)
    min_final = pd.concat([min_s2,newdf], axis = 1)
    std_final = pd.concat([std_s2,newdf], axis = 1)
    
    for i in range(0, mean_s1.shape[0]):

        mean_final.loc[mean_final["cluster"] == mean_s1.iloc[i,0], "VV" ] = mean_s1.loc[i,"VV"]
        mean_final.loc[mean_final["cluster"] == mean_s1.iloc[i,0], "VH" ] = mean_s1.loc[i,"VH"]
        mean_final.loc[mean_final["cluster"] == mean_s1.iloc[i,0], "ratio" ] = mean_s1.loc[i,"ratio"]
        mean_final.loc[mean_final["cluster"] == mean_s1.iloc[i,0], "diff" ] = mean_s1.loc[i,"diff"]
        
        max_final.loc[max_final["cluster"] == max_s1.iloc[i,0], "VV" ] = max_s1.loc[i,"VV"]
        max_final.loc[max_final["cluster"] == max_s1.iloc[i,0], "VH" ] = max_s1.loc[i,"VH"]
        max_final.loc[max_final["cluster"] == max_s1.iloc[i,0], "ratio" ] = max_s1.loc[i,"ratio"]
        max_final.loc[max_final["cluster"] == max_s1.iloc[i,0], "diff" ] = max_s1.loc[i,"diff"]
        
        min_final.loc[min_final["cluster"] == min_s1.iloc[i,0], "VV" ] = min_s1.loc[i,"VV"]
        min_final.loc[min_final["cluster"] == min_s1.iloc[i,0], "VH" ] = min_s1.loc[i,"VH"]
        min_final.loc[min_final["cluster"] == min_s1.iloc[i,0], "ratio" ] = min_s1.loc[i,"ratio"]
        min_final.loc[min_final["cluster"] == min_s1.iloc[i,0], "diff" ] = min_s1.loc[i,"diff"]
        
        std_final.loc[std_final["cluster"] == std_s1.iloc[i,0], "VV" ] = std_s1.loc[i,"VV"]
        std_final.loc[std_final["cluster"] == std_s1.iloc[i,0], "VH" ] = std_s1.loc[i,"VH"]
        std_final.loc[std_final["cluster"] == std_s1.iloc[i,0], "ratio" ] = std_s1.loc[i,"ratio"]
        std_final.loc[std_final["cluster"] == std_s1.iloc[i,0], "diff" ] = std_s1.loc[i,"diff"]
        
        
    return mean_final,max_final,min_final,std_final #''',quantile_90_final, quantile_10_final'''
    

def get_as_dataframe(y_fora, y_dentro, cols, cols_type):
    
    data_fora = pd.DataFrame(data=y_fora, columns=cols)
    data_fora  = data_fora.astype(cols_type)
    
    data_dentro = pd.DataFrame(data=y_dentro, columns=cols)
    data_dentro  = data_dentro.astype(cols_type)
    
    if ("date" in data_dentro.columns and "position" in data_dentro.columns) or ("date" in data_fora.columns and "position" in data_fora.columns) :
        data_fora = data_fora.drop(['date', 'position'], axis=1)
        data_dentro = data_dentro.drop(['date', 'position'], axis=1)
    
    return data_fora, data_dentro



def get_difference(y_fora_s1,y_dentro_s1,y_fora_s2, y_dentro_s2, cols, cols_type):
    data_diff_s1 = []
    if len(y_fora_s1) >0 and len(y_dentro_s1) >0: 
        temp_diff_s1 = y_fora_s1[:,3:].astype(float) - y_dentro_s1[:,3:].astype(float)
        diff_s1 = np.concatenate([y_fora_s1[:,2][:,None], temp_diff_s1 ],axis=1) 
    
        data_diff_s1 = pd.DataFrame(data=diff_s1, columns=["cluster", "VH", "VV", "ratio", "diff"])
        data_diff_s1  = data_diff_s1.astype({"cluster":str, "VH":float, "VV":float, "ratio":float, "diff":float})
    
    
    temp_diff_s2 = y_fora_s2[:,3:].astype(float) - y_dentro_s2[:,3:].astype(float)
    diff_s2 = np.concatenate([y_fora_s2[:,2][:,None], temp_diff_s2 ],axis=1) 
    
    data_diff_s2 = pd.DataFrame(data=diff_s2, columns=cols[2:])
    data_diff_s2  = data_diff_s2.astype(cols_type)
    
    return data_diff_s1 , data_diff_s2



def get_ratio(y_fora_s1,y_dentro_s1,y_fora_s2, y_dentro_s2, cols, cols_type):
    data_ratio_s1 = []
    if len(y_fora_s1) >0 and len(y_dentro_s1) >0: 
        temp_ratio_s1 = y_fora_s1[:,3:].astype(float) / y_dentro_s1[:,3:].astype(float)
        ratio_s1 = np.concatenate([y_fora_s1[:,2][:,None], temp_ratio_s1 ],axis=1) 
        
        data_ratio_s1 = pd.DataFrame(data=ratio_s1, columns=["cluster", "VH", "VV", "ratio", "diff"])
        data_ratio_s1  = data_ratio_s1.astype({"cluster":str, "VH":float, "VV":float, "ratio":float, "diff":float})
    
    
    temp_ratio_s2 = y_fora_s2[:,3:].astype(float) / y_dentro_s2[:,3:].astype(float)
    ratio_s2 = np.concatenate([y_fora_s2[:,2][:,None], temp_ratio_s2 ],axis=1) 
    
    data_ratio_s2 = pd.DataFrame(data=ratio_s2, columns=cols[2:])
    data_ratio_s2  = data_ratio_s2.astype(cols_type)
    
    return data_ratio_s1 , data_ratio_s2


def get_data_one_date(fgc_dir, fgc_type,date):
    
    folder = fgc_dir+"/"+fgc_type
    
    cols = ["date","position","cluster","ndvi","ndwi", "savi","ireci","sr","B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11"]    
    cols_pos = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]     
    
    s2_data, s1_data = read_data(fgc_dir, fgc_type)
    
    s2_data = s2_data[:, cols_pos]
    y_fora_s2, y_dentro_s2 = get_in_out(s2_data)
       
    sections = DataExtration.get_sections(folder,False)
    gt = DataExtration.get_ground_truth(sections, folder)
    
    
    filtered_dentro_s2 = y_dentro_s2[y_dentro_s2[:,0] == date]
    filtered_fora_s2 = y_fora_s2[y_fora_s2[:,0] == date]
    
    temp_diff_s2 = filtered_fora_s2[:,3:].astype(float) - filtered_dentro_s2[:,3:].astype(float)
    diff_s2 = np.concatenate([filtered_fora_s2[:,0:3], temp_diff_s2 ],axis=1) 
    
    ratio_s2 = filtered_fora_s2[:,3:].astype(float) / filtered_dentro_s2[:,3:].astype(float)
    ratio_s2 = np.concatenate([filtered_fora_s2[:,0:3], ratio_s2 ],axis=1) 
    
    
    final_data = []    
    for i in range(0, filtered_fora_s2.shape[0]):
        cluster = int(filtered_fora_s2[i,2][1:])
        temp_arr = []
        
        for j in range(3, filtered_fora_s2.shape[1]):
            temp_arr.append(filtered_fora_s2[i,j])
            temp_arr.append(filtered_dentro_s2[i,j])
            temp_arr.append(diff_s2[i,j])
            temp_arr.append(ratio_s2[i,j])
            
                     
        temp_arr.append(cluster)    
        temp_arr.append(int(gt[cluster]))
        
        final_data.append(temp_arr)
        
    final_data = np.asarray(final_data).astype(float)

    final_d = final_data[~np.isnan(final_data).any(axis=1)] # remover linhas que têm elemenentos Nan
    final_d = final_data[~np.isinf(final_data).any(axis=1)] 
    labels = final_data[:,-2:]
    final_data =  final_d[:,0:-2]
    
    features_names = []
    for feat in cols[3:]:
        features_names.append("fora_"+str(feat))
        features_names.append("dentro_"+str(feat))
        features_names.append("diff_"+str(feat))
        features_names.append("ratio_"+str(feat))
    

    final_data = pd.DataFrame(data=final_data, columns=features_names )
    
    features_names.append("cluster")
    features_names.append("label")
    save_to_file(folder+"/dados_"+fgc_type+"_s2_labeled_estatico.csv", final_d, features_names)


    return final_data, labels



def extract_metrics(fgc_dir, fgc_type):
    
    folder = fgc_dir+"/"+fgc_type

    cols = ["date","position","cluster","ndvi","ndwi", "savi","ireci","sr","B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11"]
    cols_type = {"date":str,"position":str,"cluster":str,"ndvi":float,"ndwi":float, "savi":float,"ireci":float,"sr":float,"B02":float, "B03":float, "B04":float, "B05":float, "B06":float, "B07":float, "B08":float, "B11":float}    
    cols_pos = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]     
    
    s2_data, s1_data = read_data(fgc_dir, fgc_type)
    s2_data = s2_data[:, cols_pos]
    
    y_fora_s2, y_dentro_s2 = get_in_out(s2_data)
    
    y_fora_s1, y_dentro_s1 = get_in_out(s1_data)
    
    
    sections = DataExtration.get_sections(folder,False)
    gt = DataExtration.get_ground_truth(sections, folder)
    
    data_fora_s1, data_dentro_s1 = get_as_dataframe(y_fora_s1, y_dentro_s1, ["date","position","cluster","VH", "VV", "ratio", "diff"], {"date":str,"position":str,"cluster":str, "VH":float, "VV":float, "ratio":float, "diff":float})
    data_fora_s2, data_dentro_s2 = get_as_dataframe(y_fora_s2, y_dentro_s2, cols, cols_type)
    
    g1 = data_fora_s1.groupby(['cluster']).count().reset_index().values
    g2 = data_dentro_s1.groupby(['cluster']).count().reset_index().values

    to_remove = np.union1d( np.asarray(g1[ g1[:,1] < 15 ][:,0]), np.asarray(g2[g2[:,1] < 15][:,0]))
    y_fora_s1 = remove_elements(y_fora_s1, to_remove)
    y_dentro_s1 = remove_elements(y_dentro_s1, to_remove)
     
    
    data_fora_s1, data_dentro_s1 = get_as_dataframe(y_fora_s1, y_dentro_s1, ["date","position","cluster","VH", "VV", "ratio", "diff"], {"date":str,"position":str,"cluster":str, "VH":float, "VV":float, "ratio":float, "diff":float})
    
 
    mean_dentro,max_dentro,min_dentro,std_dentro = metrics_(data_dentro_s1,data_dentro_s2)  

    mean_fora,max_fora,min_fora,std_fora = metrics_(data_fora_s1,data_fora_s2)


    del cols_type["date"]
    del cols_type["position"]
    
    
    #differnce
    data_diff_s1, data_diff_s2 =get_difference(y_fora_s1,y_dentro_s1, y_fora_s2,y_dentro_s2, cols, cols_type)
     
    mean_diff,max_diff,min_diff,std_diff = metrics_(data_diff_s1,data_diff_s2)
    
    
    #ratio
    data_ratio_s1, data_ratio_s2 =get_ratio(y_fora_s1,y_dentro_s1, y_fora_s2,y_dentro_s2, cols, cols_type)
     
    mean_ratio,max_ratio,min_ratio,std_ratio = metrics_(data_ratio_s1,data_ratio_s2)
    
    
    deltas_s2 = delta_diff(data_diff_s2.values, 20 )
    deltas_diff_s2 = pd.DataFrame(data=deltas_s2, columns=cols[2:])
    deltas_diff_s2  = deltas_diff_s2.astype(cols_type)
    
    deltas_s1 = delta_diff(data_diff_s1.values, 15 )
    deltas_diff_s1 = pd.DataFrame(data=deltas_s1, columns=["cluster", "VH", "VV", "ratio", "diff"])
    deltas_diff_s1  = deltas_diff_s1.astype({"cluster":str, "VH":float, "VV":float,"ratio":float, "diff":float})
    
#    mean_deltas,max_deltas,min_deltas,std_deltas,quantile_90_deltas, quantile_10_deltas = metrics_(deltas_diff_s1, deltas_diff_s2)
    mean_deltas,max_deltas,min_deltas,std_deltas = metrics_(deltas_diff_s1, deltas_diff_s2)
    
    final_data = []    
    for i in range(0, mean_dentro.shape[0]):
        cluster = int(mean_dentro.iloc[i,0][1:])
        
        temp_arr = []
        
        for j in range(1, mean_dentro.shape[1]):
            temp_arr.append(mean_dentro.iloc[i,j])
            temp_arr.append(max_dentro.iloc[i,j])
            temp_arr.append(min_dentro.iloc[i,j])
            temp_arr.append(std_dentro.iloc[i,j])
            
            temp_arr.append(mean_fora.iloc[i,j])
            temp_arr.append(max_fora.iloc[i,j])
            temp_arr.append(min_fora.iloc[i,j])
            temp_arr.append(std_fora.iloc[i,j])
            
            temp_arr.append(mean_diff.iloc[i,j])
            temp_arr.append(max_diff.iloc[i,j])
            temp_arr.append(min_diff.iloc[i,j])
            temp_arr.append(std_diff.iloc[i,j])
            
            temp_arr.append(mean_ratio.iloc[i,j])
            temp_arr.append(max_ratio.iloc[i,j])
            temp_arr.append(min_ratio.iloc[i,j])
            temp_arr.append(std_ratio.iloc[i,j])
            
            temp_arr.append(mean_deltas.iloc[i,j])
            temp_arr.append(max_deltas.iloc[i,j])
            temp_arr.append(min_deltas.iloc[i,j])
            temp_arr.append(std_deltas.iloc[i,j])
            
                     
        temp_arr.append(cluster)    
        temp_arr.append(int(gt[cluster]))
        
        final_data.append(temp_arr)
        
    final_data = np.asarray(final_data).astype(float)

    final_d = final_data[~np.isnan(final_data).any(axis=1)] # remover linhas que têm elemenentos Nan
    final_d = final_data[~np.isinf(final_data).any(axis=1)] 
    labels = final_data[:,-2:]
    final_data =  final_d[:,0:-2]
    
    features_names = []
    if len(y_fora_s1) >0 and len(y_dentro_s1) >0: 
        cols.append("VH")
        cols.append("VV")
        cols.append("S1_ratio")
        cols.append("S1_diff")
        
    for i in range(3,len(cols)):
        
        features_names.append("mean_dentro_"+cols[i])
        features_names.append("max_dentro_"+cols[i])
        features_names.append("min_dentro_"+cols[i])
        features_names.append("std_dentro_"+cols[i])

        features_names.append("mean_fora_"+cols[i])
        features_names.append("max_fora_"+cols[i])
        features_names.append("min_fora_"+cols[i])
        features_names.append("std_fora_"+cols[i])

        features_names.append("mean_diff_"+cols[i])
        features_names.append("max_diff_"+cols[i])
        features_names.append("min_diff_"+cols[i])
        features_names.append("std_diff_"+cols[i]) 
        
        features_names.append("mean_ratio_"+cols[i])
        features_names.append("max_ratio_"+cols[i])
        features_names.append("min_ratio_"+cols[i])
        features_names.append("std_ratio_"+cols[i]) 
        
        features_names.append("mean_deltas_"+cols[i])
        features_names.append("max_deltas_"+cols[i])
        features_names.append("min_deltas_"+cols[i])
        features_names.append("std_deltas_"+cols[i])  
    
    
    final_data = pd.DataFrame(data=final_data, columns=features_names )
    
    features_names.append("cluster")
    features_names.append("label")
    save_to_file(folder+"/dados_"+fgc_type+"_s2_s1_labeled.csv", final_d, features_names)
    
    
    return final_data, labels
    
    
def get_columns(n_dataset, satellite): 
    if satellite == "s1":
        return  ["VH","VV", "S1_ratio","S1_diff","cluster","label"]
    
    if n_dataset == 1:
        if satellite == "s2_s1":
            cols = ["ndvi","ndwi", "savi","ireci","sr","B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11","VH","VV", "S1_ratio","S1_diff","cluster","label"]
        elif satellite == "s2":  
            cols = ["ndvi","ndwi", "savi","ireci","sr","B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11","cluster","label"] 
   
    elif n_dataset == 2:
        if satellite == "s2_s1":
            cols = ["ndvi","ndwi", "savi","ireci","sr","VH","VV", "S1_ratio","S1_diff","cluster","label"] 
        elif satellite == "s2":
            cols = ["ndvi","ndwi", "savi","ireci","sr","cluster","label"]
        
      
    elif n_dataset == 3:
        if satellite == "s2_s1":
            cols = ["B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11","VH","VV", "S1_ratio","S1_diff","cluster","label"]
        elif satellite == "s2":
            cols = ["B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11","cluster","label"]  
        
    elif n_dataset == 4:
        cols = ["ndvi","cluster","label"] 
    
    elif n_dataset == 5:
        if satellite == "s2_s1":
            cols = ["ndvi","ndwi","B03", "B04", "B05", "B06","B07","B08","VH","VV", "S1_ratio","S1_diff","cluster","label"] 
        elif satellite == "s2":
            cols = ["ndvi","ndwi","B03", "B04", "B05", "B06","B07","B08","cluster","label"]
        
    return cols    



def get_data(folder,fgc_type, n_dataset,satellite, static):
    
    my_data = pd.read_csv(folder+"/dados_"+fgc_type+"_s2_s1_labeled.csv", sep=',', engine="python")
    if static:
        my_data = pd.read_csv(folder+"/dados_"+fgc_type+"_s2_labeled_estatico.csv", engine="python")

        
    sections = DataExtration.get_sections(folder,False)
    gt = DataExtration.get_ground_truth(sections, folder)
    
  
    #choose the features to use
    cols = get_columns(n_dataset,satellite)
    
    features_names = []
    for feat in my_data.columns:
        for col_name in cols: 
            if col_name in feat :
                features_names.append(feat)
    
    my_data= my_data.loc[:, features_names]
    my_data = my_data.values
    
    labels = my_data[:,-2:]
    final_data =  my_data[:,0:-2]
    
    final_data = pd.DataFrame(data=final_data, columns=features_names[:-2] )
    
    print(final_data.shape[1])
    return gt, final_data, labels



def add_ratio_s1(fgc_dir,fgc_type):
    file = fgc_dir+"/"+fgc_type+"/dados_"+fgc_type+"_s1.csv"
    df1=pd.read_csv(file, sep=',', engine="python")
    
    ratio = df1["VV"] / df1["VH"]
    
    dif  =  df1["VV"] - df1["VH"]
    
    new_df = pd.concat([df1,ratio], axis = 1)
    new_df = pd.concat([new_df,dif], axis = 1)
    
    os.remove(file)
    save_to_file(file, new_df.values,["date","position","cluster", "VH", "VV", "ratio", "diff"])


def start(fgc_dir):
    
    start = timer()
#
    s2_data, s1_data = cluster_data(fgc_dir, "linhas")

    with open(fgc_dir+"/linhas/dados_linhas_s2.csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(["date","position","cluster","ndvi","ndwi", "savi","ireci","sr", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11"])
        for row in s2_data:
            csv_out.writerow(row)
#    
#
    with open(fgc_dir+"/linhas/dados_linhas_s1.csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(["date","position","cluster","VH", "VV"])
        for row in s1_data:
            csv_out.writerow(row)         
##            
##            
    s2_data, s1_data = cluster_data(fgc_dir,"estradas")
##
    with open(fgc_dir+"/estradas/dados_estradas_s2.csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(["date","position","cluster","ndvi","ndwi", "savi","ireci","sr", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B11"])
        for row in s2_data:
            csv_out.writerow(row)
            
#            
    with open(fgc_dir+"/estradas/dados_estradas_s1.csv",'w',newline='') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(["date","position","cluster","VH", "VV"])
        for row in s1_data:
            csv_out.writerow(row)
#                        
##     
    print("Adding S1 ratio and difference...")        
    add_ratio_s1(fgc_dir,"linhas")
    add_ratio_s1(fgc_dir,"estradas")
##            
    
    print("Extracting final metrics linhas...") 
    extract_metrics(fgc_dir,"linhas")
##
    print("Extracting final metrics estradas...") 
    extract_metrics(fgc_dir,"estradas")


    print("Extracting data from one date for static analysis... ")
    get_data_one_date(fgc_dir, "linhas", 20180912)
    get_data_one_date(fgc_dir, "estradas", 20180912)
    
    print("fim")

    duration = timer() - start
    print("Duration: ", duration)




if __name__ == '__main__': 
    
    if len(sys.argv) != 2 :
        print("Missing arguments! Command usage:")
        print("  python DataExtraction.py '<path_to_fgci_directory>' \n")
        sys.exit()
        
    fgci_dir = sys.argv[1]

 #    fgc_dir = "../../Santarem/fgc"
    fgc_dir = "../../exp/series/clusters/data"
    
    start(fgc_dir)











