from osgeo import gdal
import os
#raster_path="C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Cardigos/Mascaras/2018/S2B_MSIL2A_20180425T112109_N0206_R037_T29TNE_20180425T165150/20180425_B03_10m_copia.tif"



base_path = "C:/Users/rafon/OneDrive/Ambiente de Trabalho/Thesis/Imagem de referencia/"


def shiftAll(dir_name, dx, dy):
    
    path = base_path + dir_name 
    
    for subdir, dirs, files in os.walk(path):
        for file in files:
            filename = os.fsdecode(file)
            
            if filename.endswith("m.jp2"):
                file_path = subdir+"/"+filename
                output_path = subdir+"/"+filename[:-4]+".tif"
                
                data = gdal.Open(file_path)
                driver = gdal.GetDriverByName("GTiff")
                dst_ds = driver.CreateCopy(output_path, data, strict=1)
                
                geo = data.GetGeoTransform()
            
                new_geo = ([geo[0]+dx, geo[1], geo[2],geo[3]+dy,geo[4],geo[5]])
                
                dst_ds.SetGeoTransform(new_geo)
                gdal.Warp(output_path,dst_ds,dstSRS='EPSG:32629')
                
                data = None
                dst_ds = None
                
            
def shiftOne(dx,dy):
    raster_path="C:\\Users\\rafon\\OneDrive\\Ambiente de Trabalho\\nova\S2A_MSIL1C_20160430T112122_N0201_R037_T29SND_20160430T112639_\\T29SND_20160430T112122_NDVI.jp2"
    
    output_path ="C:\\Users\\rafon\\OneDrive\\Ambiente de Trabalho\\nova\S2A_MSIL1C_20160430T112122_N0201_R037_T29SND_20160430T112639_\\adjusted_NDVI.tiff"
    
    data = gdal.Open(raster_path)
    driver = gdal.GetDriverByName("GTiff")
    dst_ds = driver.CreateCopy(output_path, data, strict=0)
    
    geo = data.GetGeoTransform()
    geo[0] #top left x
    geo[1] #w-e pixel resolution */
    geo[2] #0 
    geo[3] # top left y
    geo[4] #0 
    geo[5] #n-s pixel resolution (negative value)
    
    
    new_geo = ([geo[0]+dx, geo[1], geo[2],geo[3]+dy,geo[4],geo[5]])
    dst_ds.SetGeoTransform(new_geo)
    gdal.Warp(output_path,dst_ds,dstSRS='EPSG:32629')
    
    data =None
    dst_ds = None
    
    # Add output raster to canvas
    iface.addRasterLayer(output_path, "adjusted_NDVI.tiff")
    
    
shiftOne(-6,-6)
#shiftAll("S2A_MSIL2A_20190224T112111_N0211_R037_T29SPB_20190224T154746.SAFE",-3,-2)