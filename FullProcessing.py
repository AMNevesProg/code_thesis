# -*- coding: utf-8 -*-
"""
Created on Sun Dec 21 17:49:53 2019

Realiza toda a cadeia de processamento das faixas, isto é, dados os shapefiles originais das fgci de um concelho e as imagens dos satélites Sentinel-1 e Sentinel-2,
realiza todos os passos de pre-processamento necessários e extrai a informação das séries temporais

@author: Ricardo
"""



import os,sys
import ProcessFGCFiles
import ExtractClustersData
import DataExtration
from timeit import default_timer as timer


#def start():

    
if __name__ == '__main__': 
    
    if len(sys.argv) != 4 :
        print("Missing arguments! Command usage:\n")
        print("   python FullProcessing.py '<path_to_fgci_shapefile.shp>' '<path_to_s2_products_folder>' '<path_to_s1_products_folder>' ")
        sys.exit()


    fgci_path = sys.argv[1]
    s2_path = sys.argv[2]
    s1_path = sys.argv[3]

    fgc_dir = os.path.dirname(os.path.realpath(fgci_path))

    start = timer()


   

    print("WARNING: The full processing can take several hours (most likely more than 24h)")
    
    print("Start Processing the shapefiles")
#    ProcessFGCFiles.start(fgci_path)
    
    print("Starting cutting images...")
#    ExtractClustersData.start(fgc_dir, s1_path, s2_path)
    
    print("Stating data extraction...")
    DataExtration.start(fgc_dir)
    print("")
    
    
    duration = timer()-start
    print("Full Processing Duration: ",duration)
    
#    start("../../Santarem/fgc", "estradas")
    
    