# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 19:20:20 2019

@author: Ricardo
"""

import numpy
import os
from osgeo import gdal

#projeção das imagens do sentinel2 
proj = 'PROJCS["WGS 84 / UTM zone 29N",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433],AUTHORITY["EPSG","4326"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-9],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","32629"]]'
    

def writeImageToFile(img, x_size, y_size, geoTransf, file):
    
    driver =  gdal.GetDriverByName("GTiff")
    out = driver.Create(file,x_size,  y_size,1, gdal.GDT_Float32)
    
    if out is None:
        print('Could not create file:'+file)
        
    out.SetGeoTransform(geoTransf)
    out.SetProjection(proj)
    
    out.GetRasterBand(1).WriteArray(img)
    out.GetRasterBand(1).SetNoDataValue(0)
    
    # flush data to disk
    out.FlushCache()
    out = None



def SAVI(imNIR,imRED,outfile):
    nir= gdal.Open(imNIR)
    band1 = nir.GetRasterBand(1)
    nirArr = numpy.array(band1.ReadAsArray(0,0,nir.RasterXSize,nir.RasterYSize))
    
    red = gdal.Open(imRED)
    band2 = red.GetRasterBand(1)
    redArr = numpy.array(band2.ReadAsArray(0,0,red.RasterXSize,red.RasterYSize))
    
    #tem de ser usado o QUANTIFICATION_VALUE que esta o ficheiro xml, por agora é 10000 senao os valores do SAVI nao estão corretos
    new_nir = nirArr.astype(float)/10000.0
    new_red = redArr.astype(float)/10000.0
    
    savi = (1 + 0.5)* ( ( new_nir - new_red ) / (new_nir + new_red + 0.5) )
    where_are_NaNs = numpy.isnan(savi)
    savi[where_are_NaNs] = 0
    
    if outfile != None or outfile != "":
        writeImageToFile(savi,nir.RasterXSize,  nir.RasterYSize, nir.GetGeoTransform(), outfile )
    
    return savi    

def NDWI(imNIR,imSWIR,outfile):
    nir= gdal.Open(imNIR)
    band1 = nir.GetRasterBand(1)
    nirArr = numpy.array(band1.ReadAsArray(0,0,nir.RasterXSize,nir.RasterYSize))
    
    swir = gdal.Open(imSWIR)
    band2 = swir.GetRasterBand(1)
    swirArr = numpy.array(band2.ReadAsArray(0,0,swir.RasterXSize,swir.RasterYSize))
    
    ndwi = (nirArr.astype(float) - swirArr.astype(float))/(swirArr + nirArr)
    where_are_NaNs = numpy.isnan(ndwi)
    ndwi[where_are_NaNs] = 0
    
    if outfile != None or outfile != "":
        writeImageToFile(ndwi,nir.RasterXSize,  nir.RasterYSize, nir.GetGeoTransform(), outfile)

    return ndwi
    
def IRECI(imRE1, imRE2, imRE3, imRED, outfile):
    re1= gdal.Open(imRE1)
    band1 = re1.GetRasterBand(1)
    re1Arr = numpy.array(band1.ReadAsArray(0,0,re1.RasterXSize,re1.RasterYSize))
    
    re2= gdal.Open(imRE2)
    band2 = re2.GetRasterBand(1)
    re2Arr = numpy.array(band2.ReadAsArray(0,0,re2.RasterXSize,re2.RasterYSize))
    
    re3= gdal.Open(imRE3)
    band3 = re3.GetRasterBand(1)
    re3Arr = numpy.array(band3.ReadAsArray(0,0,re3.RasterXSize,re3.RasterYSize))
    
    red = gdal.Open(imRED)
    band4 = red.GetRasterBand(1)
    redArr = numpy.array(band4.ReadAsArray(0,0,red.RasterXSize,red.RasterYSize))
    
    
    new_re1 = re1Arr.astype(float)/10000.0
    new_re2 = re2Arr.astype(float)/10000.0
    new_re3 = re3Arr.astype(float)/10000.0
    new_red = redArr.astype(float)/10000.0
    
    
    ireci = (new_re3 - new_red) / (new_re1 / new_re2)
    where_are_NaNs = numpy.isnan(ireci)
    ireci[where_are_NaNs] = 0
    
    
    if outfile != None or outfile != "":
        writeImageToFile(ireci,red.RasterXSize,  red.RasterYSize, red.GetGeoTransform(), outfile )

    return ireci

def simpleRatio(imNIR,imRED, outfile):
    nir= gdal.Open(imNIR)
    band1 = nir.GetRasterBand(1)
    nirArr = numpy.array(band1.ReadAsArray(0,0,nir.RasterXSize,nir.RasterYSize))
    
    red = gdal.Open(imRED)
    band2 = red.GetRasterBand(1)
    redArr = numpy.array(band2.ReadAsArray(0,0,red.RasterXSize,red.RasterYSize))
    
    sr = (nirArr.astype(float)/10000.0) /(redArr.astype(float)/10000.0)
    where_are_NaNs = numpy.isnan(sr)
    sr[where_are_NaNs] = 0
    
    if outfile != None or outfile != "":
        writeImageToFile(sr,nir.RasterXSize,  nir.RasterYSize, nir.GetGeoTransform(), outfile )
    
    return sr



def NDVI(imNIR, imRED, outfile): 
    nir= gdal.Open(imNIR)
    band1 = nir.GetRasterBand(1)
    nirArr = numpy.array(band1.ReadAsArray(0,0,nir.RasterXSize,nir.RasterYSize))
    
    red = gdal.Open(imRED)
    band2 = red.GetRasterBand(1)
    redArr = numpy.array(band2.ReadAsArray(0,0,red.RasterXSize,red.RasterYSize))
    
    
    ndvi = (nirArr.astype(float) - redArr.astype(float))/(redArr.astype(float) + nirArr.astype(float))
    where_are_NaNs = numpy.isnan(ndvi)
    ndvi[where_are_NaNs] = 0
    
    
    if outfile != None or outfile != "":
        writeImageToFile(ndvi,nir.RasterXSize,  nir.RasterYSize, nir.GetGeoTransform(), outfile )
    
#    driver =  gdal.GetDriverByName("GTiff")
#    out = driver.Create(outfile,nir.RasterYSize,  nir.RasterXSize,1, gdal.GDT_Float32)
#    
#    if out is None:
#        print('Could not create file:'+outfile)
#        
#    # write the data
#    print(nir.GetProjection())
#    out.SetGeoTransform(nir.GetGeoTransform())
#    out.SetProjection(proj)
#    
#    out.GetRasterBand(1).WriteArray( ndvi)
#    out.GetRasterBand(1).SetNoDataValue(0)
#    
#    # flush data to disk
#    out.FlushCache()
#    out = None
        
    return ndvi
        

def indexes_to_all(path):
    for subdir, dirs, files in os.walk(path):
        
        b08 = ""
        b04 = ""
        b05 = ""
        b06 = ""
        b07 = ""
        b11 = ""
        
        ndvi = False
        sr = False
        ireci = False
        ndwi = False
        savi = False
        
        for file in files:
            
            file_path = subdir+'/'+file
            
            
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B08_10m" in file: 
                b08 = file_path
            
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B04_10m" in file:
                b04 = file_path
            
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B05_10m" in file:
                b05 = file_path
                
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B06_10m" in file:
                b06 = file_path
                
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B07_10m" in file:
                b07 = file_path
                
            if (file.endswith(".tiff") or file.endswith(".jp2")) and "B11_10m" in file:
                b11 = file_path   
            
    
            if b08 != "" and b04 != "" and sr == False and ndvi == False and savi == False:
                ndvi = sr = savi = True
                new_file = b08.replace("_B08_", "_NDVI_")
                NDVI(b08, b04,new_file )

                new_file = b08.replace("_B08_", "_SR_")
                simpleRatio(b08,b04,new_file)
 
                new_file = b08.replace("_B08_", "_SAVI_")
                SAVI(b08,b04,new_file)
                
            if b05 != "" and b06 != "" and b07 != "" and b04 != "" and ireci ==False:
                ireci = True
                new_file = b04.replace("_B04_","_IRECI_")
                IRECI(b05,b06,b07,b04, new_file)
              
                
            if b08 != "" and b11 != "" and ndwi == False:
                ndwi = True
                new_file = b08.replace("_B08_","_NDWI_")
                NDWI(b08,b11, new_file)
            
            if ndvi == True and ireci == True and ndwi == True:
                print("done!")
            



if __name__ == '__main__': 

    
    
    indexes_to_all("../Cardigos/Sentinel-2")



















