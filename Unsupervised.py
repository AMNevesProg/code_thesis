# -*- coding: utf-8 -*-
"""
Created on Sat Jan  4 10:01:42 2020

@author: Ricardo
"""

from osgeo import ogr, gdal
from sklearn.cluster import KMeans,DBSCAN
import itertools
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import f1_score
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold
from sklearn.model_selection  import train_test_split
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import os,sys
import numpy as np
import pandas as pd
import DataExtration
from sklearn import preprocessing

import csv



gdal.AllRegister()
gdal.UseExceptions()
ogr.DontUseExceptions()
  

def formatFloat(value):
    return "{0:.7f}".format(value)              


def metrics2(pred,labels):
    accuracy = accuracy_score(labels,pred)
    adjusted_rand = adjusted_rand_score(labels, pred)
    kappa = cohen_kappa_score(labels,pred)
    mae = mean_absolute_error(labels,pred)
    F1 = f1_score(labels,pred, average ="macro")
    
    return accuracy, adjusted_rand, kappa, mae, F1    


def plot_indexes(rand_ix,title):   
    plt.figure(figsize=(8,6))
    plt.title(title)
    plt.plot(rand_ix[:,0],rand_ix[:,1], 'r', label="accuracy") #Rand
    plt.plot(rand_ix[:,0],rand_ix[:,2], 'y', label="Adjusted_Rand") #Adjusted_Rand
    plt.plot(rand_ix[:,0],rand_ix[:,3], 'b', label="kappa") #Precision
    plt.plot(rand_ix[:,0],rand_ix[:,4], 'k', label="mean_absolute_error") #Recall
    plt.plot(rand_ix[:,0],rand_ix[:,5], 'g', label="F1") #F1
#    plt.plot(rand_ix[:,0],rand_ix[:,6], 'm', label="Silhouette") #silhouette_score
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#    plt.savefig(title+".png", dpi=200,bbox_inches='tight')
    plt.show


def k_means(data,k):
    #cria um KMeans com k centroids
    
    kmeans = KMeans(n_clusters=k).fit(data)
    labels = kmeans.predict(data)
 
    centroids = kmeans.cluster_centers_

    return labels,centroids

def dbscan(data,k):
    #cria um KMeans com k centroids
    db = DBSCAN(eps=k).fit(data)
    labels = db.fit_predict(data)
 
#    centroids = kmeans.cluster_centers_

    return labels,""

def test_KMeans(data):
    data = pd.DataFrame(data =np.asarray(data) )
    data.columns = ['cluster_id', 'ndvi','out','diff' ]
    data  = data.astype({"cluster_id": str, "ndvi": float, 'out':float, 'diff':float})
    
    
    data = data[(data.ndvi != 0.0)]
    
    data = data.groupby(['cluster_id']).mean().reset_index()
    data = data.values #.astype(float)
    
    cluster_id = data[:,0]
    data = data[:,1:]
    
#    pairs = list(itertools.combinations(range(data.shape[0]),2))
#    pairs = np.asarray(pairs)
        
    labels,centroids = k_means(data,3)
        
    print("kmean")    
    
    return np.concatenate([cluster_id[:,None],labels[:,None]],axis=1)



def save_cluster_classification(labels, out_file, folder):
    
    fgc_shp = ogr.Open(folder+"/points_inout_clusters.shp")

    lyr = fgc_shp.GetLayer()
    
    driver=ogr.GetDriverByName('ESRI Shapefile')

    ds=driver.CreateDataSource(folder+"/"+out_file)
    
    cluster_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), geom_type=ogr.wkbPoint )    
    
    newfield1 = ogr.FieldDefn("OLD_ID", ogr.OFTString)
    newfield2 = ogr.FieldDefn("CLUSTER_ID", ogr.OFTReal)
    cluster_lyr.CreateField(newfield1)
    cluster_lyr.CreateField(newfield2)
    
    for feat in lyr:
        geom = feat.GetGeometryRef()
            
        i_cluster_id = feat.GetFieldIndex("CLUSTER_ID")
        cluster_id = feat.GetFieldAsString(i_cluster_id)
        i_type = feat.GetFieldIndex("TYPE")
        f_type = feat.GetFieldAsString(i_type)

#        temp = labels[labels[:,0] == "c"+str(int(cluster_id)),1]
        temp = labels[labels[:,0] == int(cluster_id),1]

        if temp.shape[0] > 0  and f_type == "fgc":
#            new_cluster_id = int( labels[labels[:,0] == "c"+str(int(cluster_id)) ,1] ) 
            new_cluster_id = int( labels[labels[:,0] == int(cluster_id) ,1] ) 
            
            new_feat = ogr.Feature(cluster_lyr.GetLayerDefn())
            new_feat.SetGeometry(geom)
            new_feat.SetField("OLD_ID",cluster_id)
            new_feat.SetField("CLUSTER_ID",new_cluster_id)
            cluster_lyr.CreateFeature(new_feat)
            new_feat = None
            
    
    cluster_lyr = None
    lyr = None
    ds = None    
    


#uma vez que as labels do kmeans são definidas aleatoriamente, é necessário fazer a substituição para poder calcular
# as métricas. Esta subsituição feita nesta função é especifica para apenas este conjunto de dados analisado
def replace_labels(labels):
    
    count_0 = len(labels[labels[:,1] == 0]) 
    count_1 = len(labels[labels[:,1] == 1])
    count_2 = len(labels[labels[:,1] == 2])
    
    arr = [count_0, count_1, count_2]
    
    interv = arr.index(max(arr))
    talvez = arr.index(min(arr))
       
    new_labels = labels.copy()
    
    new_labels[labels[:,1] == interv ,1]= 1.0 
    new_labels[labels[:,1] == talvez ,1] = 2.0
    new_labels[(labels[:,1] != talvez) & (labels[:,1] != interv),1 ] = 0.0
    
    return new_labels    


def get_fgc_data(fgc_dir, fgc_type):
    
    s2_data,s1_data = DataExtration.read_data(fgc_dir,fgc_type)
    
    y_fora = s2_data[s2_data[:,1] == "out"]
    y_dentro = s2_data[s2_data[:,1] == "in"]
    
    to_remove = np.setxor1d(y_fora[:,2], y_dentro[:,2])
    
    y_fora = DataExtration.remove_elements(y_fora, to_remove)
    y_dentro = DataExtration.remove_elements(y_dentro, to_remove)
    
    return y_fora, y_dentro



def confusion_matrix_values(labels, gt):
    
    TP = FN = FP = TN = 0
    for pred in labels:
        cluster_id = pred[0]
        g_t = int(gt[int(float(cluster_id))])
        label = int(pred[1])
        
        if g_t == label and g_t == 1:
            TP = TP +1
        elif g_t == label and g_t == 0:
            TN = TN +1
        elif g_t == 1 and label == 0:
            FN = FN +1
        elif g_t == 0 and label == 1:
            FP = FP +1
        
    return (TP, FP, TN ,FN) 


if __name__ == '__main__':  
    
    
    if len(sys.argv) != 3 :
        print("Missing arguments! Command usage:")
        print("  python DataExtraction.py '<path_to_fgci_directory>' <type_of_fgci> \n")
        print("    type_of_fgci: 'estradas' or 'linhas'")

        sys.exit()
    
    fgci_dir = sys.argv[1]
    fgc_type = sys.argv[2]
    
    
    fgc_dir = "../../exp/series/clusters/data"
    fgc_type = "estradas"
    
    
    y_fora, y_dentro = get_fgc_data(fgc_dir,fgc_type)

    
    # Apenas imagens do Senintel-2 de 12/09/2019
    y_fora = y_fora[y_fora[:,0] == 20180912 ] 
    y_dentro = y_dentro[y_dentro[:,0] == 20180912 ] 

    #diferença do ndvi entre exterior da faixa e o interior
    diff_ndvi = y_fora[:,3].astype(float) - y_dentro[:,3].astype(float)
    
    data = np.concatenate([y_fora[:,2:4],y_dentro[:,3][:,None].astype(float)],axis=1)
    data = np.concatenate([data,diff_ndvi[:,None]],axis=1)
    
    
    labels_inout = test_KMeans(data[:,:])

    
    sections = DataExtration.get_sections(fgc_dir+"/"+fgc_type, True)
    gt = DataExtration.get_ground_truth(sections,fgc_dir+"/"+fgc_type)
    

    filtered_labels = []
    for x in sections:
        cluster_id = "c"+str(x)
        cluster_id_int = int(x)
        
        if len(labels_inout[labels_inout[:,0] == cluster_id][:,1]) > 0:
            labels_inout[labels_inout[:,0] == cluster_id]
            filtered_labels.append((cluster_id_int,labels_inout[labels_inout[:,0] == cluster_id][:,1][0]))
    
    filtered_labels = np.asarray(filtered_labels)
    
    filtered_gt = []
    for x in filtered_labels:
#        cluster_id = int(x[0][1:])        
        cluster_id = int(x[0])        
        filtered_gt.append(gt[cluster_id])
   
    filtered_gt = np.asarray(filtered_gt)

    filtered_labels = replace_labels(filtered_labels)

    print(classification_report(filtered_gt,filtered_labels[:,1].astype(int)))
    print("(TP, FP, TN, FN)")
    print(confusion_matrix_values(filtered_labels, gt))
    
    #Saves the result in a shapefile for visualization
    save_cluster_classification(filtered_labels,"KMeans_ndvi_diff_0912_3clusters.shp", fgc_dir+"/"+fgc_type)
    
    
    

