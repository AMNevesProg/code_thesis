# README

### AMBIENTE DE EXECUÇÃO

Todo o desenvolvimento foi feito num computador portátil com o sistema operativo windows 10.
As características do computador são as seguintes:

* **CPU:** Intel Core i7-8750H Hexa Core, 2.20 GHz até 4.10 GHz
* **DISCO:** SSD 256GB NVMe PCIe + 1TB HDD
* **RAM:** 16GB DDR4 2666MHz
* **GPU:** NVIDIA GeForce GTX 1060 6GB GDDR5

Toda a implementação foi realizada em python, através do Anaconda. Todas as bilbliotecas usadas estão disponíveis no Anaconda e como tal são de fácil instalação.

Foram usadas as bibliotecas:

* qgis (https://anaconda.org/conda-forge/qgis)
	* contém bibliotecas para a manipulação de ficheiros vetorais e imagens raster
* gdal (https://anaconda.org/conda-forge/gdal)
	* manipulação de ficheiros raster
* rasterio (https://anaconda.org/conda-forge/rasterio)
	* manipulação de ficheiros raster
* fiona (https://anaconda.org/conda-forge/fiona)
	* manipulação de ficheiros vetoriais
* xgboost (https://anaconda.org/conda-forge/xgboost)
	* algoritmo de aprendizagem automática que usa paralelização através do CPU ou da GPU
* cudatoolkit (https://anaconda.org/anaconda/cudatoolkit)
	* Necessário para o xgboost e requer a instalação do CUDA (https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html)


Como alguns dos scripts recorrem a paralelização para melhorar os tempos de execução, **todos os scripts devem de ser executados na linha de comandos**.

### SCRIPTS
* SAR_Preprocessamento
	* Contém um script .bat que realiza o pré-processamento dos produtos do Sentinel-1. Os passos do pré-processamento estão descritos no ficheiro *SAR_preprocessing_graph.xml* que foi gerado usando a ferramenta SNAP (https://step.esa.int/main/toolboxes/snap/). A informação sobre como correr o script está no ficheiro *Readme.txt*

* concavehull
	* Contém os ficheiros necessários para correr o algoritmo concavehull. Implementação do algortimo usadao: https://github.com/sebastianbeyer/concavehull

* *ProcessFGCFiles.py*
	* Realiza todo o pré-processamento dos *shapefiles* das FGCI:
		* separação dos diferentes tipos de faixas (estradas,linhas elétricas e habitações)
		* criação de *buffers* exteriores às faixas com 20m
		* remoção de áreas que se sobrepõem umas às outras
		* divisão das faixas em secções com aproximadamente a mesma dimensão ( usa o script *CreateSections.py*)
		* intersecção das secções com as FGCI e com os *buffers*, guardando o resultado

* *CreateSections.py*
	* Realiza a divisão das faixas em secções com aproximadamente a mesma dimensão. Usa o algoritmo *ConcaveHull.py* 

* *Geometries_FGC.py*
	* Contém várias funções úteis para a manipulação de shapefiles (intersecções, recortes, uniões, *buffers*, etc)
	
* *ExtractClustersData.py*
	* Realiza a extração da informação das imagens dos satélites (Sentinel-1 e Sentinel-2), para cada uma das secções (*buffer* e interior da FGCI)
	* Existe uma verificação se as imagens foram previamente alinhadas, reamostradas e se os índices de vegetação já foram calculados, realizando qualquer pré-processamento que estteja em falta.

* *EstimateIntervention.py*
	* Realiza uma estimativa da data de intervenção de algumas secções ao longo das faixas das estradas.

* *DataExtration.py*
	* Usando as imagens das secções que foram extraídas com script *ExtractClustersData.py*, realiza a extração de métricas (min, max, média, desvio padrão) das várias imagens, por seccção, e guarda o resultado em ficheiros .csv. De todos os passos de pré-processamento e extração de dados este é o que demora mais tempo.

* *FullProcessing.py*
	* Realiza toda a cadeia de processamento necessário para analisar as FGCI. Isto consiste na execução sequencial de alguns dos scripts mencionados anteriormente.
	
* *AdjustGeo.py*
	* Script não usado na cadeia de processamento, mas que foi usado para realizar os alinhamentos iniciais das imagens satélite da imagem de referência ( informação dos deslocamentos feitos está no ficheiro *deslocamentos.txt* ). Pode ser útil caso seja necessário ajustar "manualmente" a posição geográfica de alguma imagem. 
	
* *imageAlign.py*
	* Realiza o alinhamento de novas imagens satélite, tendo por base a imagem de referência criada. **É necessário alterar o caminho da imagem de referência, que está no script.**

* *resampleTo10m.py*
	* Realiza a reamostragem de algumas imagens que têm resolução espacial de 20m para 10m. Isto porque alguns índices de vegetação utilizam estas imagens, sendo necessário a reamostragem para realizar os cálculos.

* *Supervised.py*
	* Executa os algoritmos de *Machine Learning* usando diferentes conjuntos e combinações de dados (necessita dos shapefiles com a *ground truth*.
	
* *Unsupervised.py*
	* Executa o algoritmo *KMeans* com os dados das faixas ao longo das estradas, escrevendo o resultado do agrupamento num ficheiro vetorial.

* *ThresholdClassifier.py*
	* Realiza uma classificação das faixas recorrendo a séries temporais de índices de vegetação e à imposição de um limite.


### EXECUÇÃO


Antes de extrair os dados das imagens satélite (i.e. antes de executar o *ExtractClustersData.py* ou o *FullProcessing.py*) é necessário realizar o pré-processamento das imagens do Sentinel-1 recorrendo o *script.bat*.

O processamento completo da informação vetorial das faixas de um dado Município, está dependente deste ficheiro ter sido corretamente criado pelo Município. Em alguns casos pode não ser possível preparar e analisar a informação vetorial das faixas ao redor de habitações.

Para o caso específico de Mação existem ficheiros que contêm a ground truth (dentro da pasta Ground_Truth), que devem substituir ficheiros gerados anteriormente. Após a substituição devem ser executados novamente os scripts *ExtractClustersData.py* e *DataExtration.py*

Toda a informação extraída é guardada em pastas, dentro do diretório onde se econtra o *shapefile* das FGCI do concelho que está a ser analisado.

	