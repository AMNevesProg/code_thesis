# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 13:31:17 2019

@author: Ricardo
"""

import os,re
from osgeo import ogr, gdal
from sklearn.cluster import KMeans,MiniBatchKMeans
import Geometries_FGC
import numpy as np
from sklearn import preprocessing
from timeit import default_timer as timer
import imageAlign



def save_clusters(points, file):
    points = dict(zip(points[:,0], points[:,-1:]))
    
    points_shp = ogr.Open(file)
    lyr = points_shp.GetLayer()

    out = file[:-4]+"_clusters.shp"
    
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(out)    
    clusters_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), geom_type=ogr.wkbPoint )    
    
    newfield = ogr.FieldDefn("TYPE", ogr.OFTString)
    clusters_lyr.CreateField(newfield) 
    newfield = ogr.FieldDefn("CLUSTER_ID", ogr.OFTInteger64)
    clusters_lyr.CreateField(newfield)
#    
    for feature in lyr:
        geom = feature.GetGeometryRef()
        
        i_type = feature.GetFieldIndex("TYPE")
        f_type = feature.GetFieldAsString(i_type)
        
        new_feat = ogr.Feature(clusters_lyr.GetLayerDefn())
        new_feat.SetGeometry(geom)
        new_feat.SetField("TYPE", f_type )

#        cluster_id = int(points[points[:,0]==str(feature.GetFID()),-1:])
        cluster_id = int(points[str(feature.GetFID())])
        
        new_feat.SetField("CLUSTER_ID", cluster_id)
        
        clusters_lyr.CreateFeature(new_feat)
        
        new_feat = None
    


def get_points_lat_long(file):
#    file = "../../exp/series/clusters/data/temp/points_inout.shp"
    points_shp = ogr.Open(file)
    points = points_shp.GetLayer()
    
    points_coords = []
    for feature in points:
        
        geom = feature.GetGeometryRef()
        i_type = feature.GetFieldIndex("TYPE")
        f_type = feature.GetFieldAsString(i_type)
        
        points_coords.append((feature.GetFID(), f_type, geom.GetX(), geom.GetY()))
   
    
    points_coords= np.asarray(points_coords)
    
    points = None
    points_shp = None

    return points_coords

def create_clusters(file_dir, fgc):
    
    points = get_points_lat_long(file_dir+"/"+fgc+"/points_inout.shp")
   
    # K calculation using de total of points/pixels in the FGC
    if fgc == "estradas" or fgc == "linhas" :
        k = int(points.shape[0] / 40) # 40 points per section
    if fgc == "habitacoes" or fgc == "redeprimaria":
        k = int(points.shape[0] / 100) # 100 points per section
        
    
    data = points[:,2:]
    print("Creating clusters")
    start = timer()

    labels = MiniBatchKMeans(n_clusters=k, n_init = 15, init_size=3*k).fit_predict(data)
    points = np.append(points, labels[:,None], axis=1)
    
    
    print("Saving to file")
    save_clusters(points,file_dir+"/"+fgc+"/points_inout.shp")
    

    duration = timer() - start 
    print("duration:", duration  )
    


def get_ref_image(tile):
    
    ref_image_path = imageAlign.ref_images_path
    
    for subdir, dirs, files in os.walk(ref_image_path):
        for file in files:
            if ".SAFE" in subdir:
                file_path = subdir+'/'+file
                product = re.findall(r"[\\/]{1}S2.*\.SAFE", str(subdir))[0][1:-1]
                prod_tile = product.split("_")[5]
                
                if tile in prod_tile and "_B08_10m." in file:
                    ref_img= file_path    #"../Imagem de referencia/S2A_MSIL2A_20190214T112151_N0211_R037_T29SND_20190214T121343.SAFE/GRANULE/L2A_T29SND_A019053_20190214T112801/IMG_DATA/R10m/T29SND_20190214T112151_B08_10m.tif"
    
    return ref_img


#Extracts points from inside and the outside buffer of the FGCI 
def extract_points_fgc_and_buffer(file_dir, fgc ):
    #Uses the reference image
    
    ref_image = get_ref_image("SND") 

    image = gdal.Open(ref_image)
    proj = image.GetProjectionRef()
    
    Geometries_FGC.reproject_file(file_dir+"/"+fgc+"/cleaned_buffer_20m_merged.shp", proj, file_dir+"/"+fgc+"/temp_buffer_20m_merged.shp")
    Geometries_FGC.cut_v2(ref_image, file_dir+"/"+fgc+"/temp_buffer_20m_merged.shp", file_dir+"/"+fgc+"/temp_buff_pixeis.tiff")
    Geometries_FGC.raster_to_points(file_dir+"/"+fgc+"/temp_buff_pixeis.tiff", file_dir+"/"+fgc+"/points_buff.shp","buffer")
#    
    
    Geometries_FGC.reproject_file(file_dir+"/"+fgc+"/merged.shp", proj, file_dir+"/"+fgc+"/temp_merged.shp")
    Geometries_FGC.cut_v2(ref_image, file_dir+"/"+fgc+"/temp_merged.shp", file_dir+"/"+fgc+"/temp_fgc_pixeis.tiff")
    Geometries_FGC.raster_to_points(file_dir+"/"+fgc+"/temp_fgc_pixeis.tiff",file_dir+"/"+fgc+"/points_fgc.shp", "fgc")
      
    
#    proj = 'PROJCS["ETRS89_Portugal_TM06",GEOGCS["GCS_ETRS_1989",DATUM["ETRS_1989", SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0], UNIT["Degree",0.017453292519943295]], PROJECTION["Transverse_Mercator"], PARAMETER["latitude_of_origin",39.66825833333333],PARAMETER["central_meridian",-8.133108333333334],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]' 
#    Geometries_FGC.reproject_file("../../exp/series/clusters/data/"+folder+"/points_buff.shp", proj,"../../exp/series/clusters/data/"+folder+"/points_buff_.shp")
#    Geometries_FGC.reproject_file("../../exp/series/clusters/data/"+folder+"/points_fgc.shp", proj,"../../exp/series/clusters/data/"+folder+"/points_fgc_.shp")
    Geometries_FGC.merge_points_files(file_dir+"/"+fgc+"/points_buff.shp",file_dir+"/"+fgc+"/points_fgc.shp",file_dir+"/"+fgc+"/points_inout.shp")


#Recieves a file with clusters of points, and converts the clusters into polygons    
def create_clusters_polygons(clusters,outfile):
    #projecção dos shapefiles das fgc 
    proj = 'PROJCS["ETRS89_Portugal_TM06",GEOGCS["GCS_ETRS_1989",DATUM["ETRS_1989", SPHEROID["GRS_1980",6378137,298.257222101]],PRIMEM["Greenwich",0], UNIT["Degree",0.017453292519943295]], PROJECTION["Transverse_Mercator"], PARAMETER["latitude_of_origin",39.66825833333333],PARAMETER["central_meridian",-8.133108333333334],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]'
    #projecção das imagens sentinel 2
#    proj =  'PROJCS["WGS 84 / UTM zone 29N",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-9],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","32629"]]'
    fgc_shp = ogr.Open(clusters)
    lyr = fgc_shp.GetLayer()
    

    clusters = {}

    #extrair os pontos que pertencem ao mesmo cluster
    for feature in lyr:
        geom = feature.GetGeometryRef()
        i_cluster_id = feature.GetFieldIndex("CLUSTER_ID")
        cluster_id = feature.GetFieldAsString(i_cluster_id)                      
#        print(cluster_id)
        if cluster_id not in clusters:
            clusters[cluster_id] = []
     
        clusters[cluster_id].append((geom.GetX(),geom.GetY()))

            
    print("creating polygons")        
    driver=ogr.GetDriverByName('ESRI Shapefile')
    ds=driver.CreateDataSource(outfile)    
    points_lyr = ds.CreateLayer('temp', lyr.GetSpatialRef(), geom_type=ogr.wkbPolygon )
    
    newfield = ogr.FieldDefn("CLUSTER_ID", ogr.OFTReal)
    points_lyr.CreateField(newfield)
    
    for cluster_id in clusters:
        geomcol = ogr.Geometry(ogr.wkbGeometryCollection)
        points = np.asarray(clusters[cluster_id])

#        print("cluster: "+cluster_id)
        for p in points:
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(p[0], p[1])
            geomcol.AddGeometry(point)
    
        convexhull = geomcol.ConvexHull()   
        new_feat = ogr.Feature(points_lyr.GetLayerDefn())
        new_feat.SetGeometry(convexhull.Buffer(3.5))
        new_feat.SetField("CLUSTER_ID", cluster_id )
        
        points_lyr.CreateFeature(new_feat)
        
        new_feat = None
        
    points_lyr = None
    ds = None
    
    Geometries_FGC.reproject_file(outfile,proj, outfile.replace("temp_", ""))
    

if __name__ == '__main__':    
#
#    extract_points_fgc_and_buffer()
#    
    print("hi")
    
#    create_clusters(2994)
#    
#    Geometries_FGC.create_clusters_polygons("../../exp/series/clusters/data/temp_habi/points_inout_clusters.shp", "../../exp/series/clusters/data/temp_habi/polygons.shp")
#    



